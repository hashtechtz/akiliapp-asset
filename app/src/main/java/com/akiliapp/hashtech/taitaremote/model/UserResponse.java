package com.akiliapp.hashtech.taitaremote.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class UserResponse implements Parcelable {


    String lastValidLatitude;
    String lastValidLongitude;
    String status_lock;
    String vin, course;
    String speed;
    String plate_number;


    public String getLastValidLatitude() {
        return lastValidLatitude;
    }

    public void setLastValidLatitude(String lastValidLatitude) {
        this.lastValidLatitude = lastValidLatitude;
    }

    public String getAngle() {
        return course;
    }

    public void setAngle(String course) {
        this.course = course;
    }

    public String getLastValidLongitude() {
        return lastValidLongitude;
    }

    public void setLastValidLongitude(String lastValidLongitude) {
        this.lastValidLongitude = lastValidLongitude;
    }

    public String getStatus_lock() {
        return status_lock;
    }

    public void setStatus_lock(String status_lock) {
        this.status_lock = status_lock;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }


    public String getPlateNumber() {
        return plate_number;
    }

    public void setPlateNumber(String plate_number) {
        this.plate_number = plate_number;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.lastValidLatitude);
        dest.writeString(this.lastValidLongitude);
        dest.writeString(this.course);
        dest.writeString(this.speed);
        dest.writeString(this.plate_number);
    }

    public UserResponse() {
    }

    protected UserResponse(Parcel in) {
        this.lastValidLatitude = in.readString();
        this.lastValidLongitude = in.readString();
        this.course = in.readString();
        this.course = in.readString();
        this.plate_number = in.readString();
    }

    public static final Creator<UserResponse> CREATOR = new Creator<UserResponse>() {
        @Override
        public UserResponse createFromParcel(Parcel source) {
            return new UserResponse(source);
        }

        @Override
        public UserResponse[] newArray(int size) {
            return new UserResponse[size];
        }
    };

}
