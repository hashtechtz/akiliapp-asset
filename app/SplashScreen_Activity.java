package com.akiliapp.hashtech.akiliapp_release;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class SplashScreen_Activity extends AppCompatActivity {

    private ImageView logo1;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        logo1 = findViewById(R.id.logo);
        textView = findViewById(R.id.title1);


        Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.my_transition);
        logo1.startAnimation(myAnim);
        textView.startAnimation(myAnim);


        final Intent i = new Intent(this,MainActivity.class);


        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(3000);
                    SharedPreferences shared = getSharedPreferences("Mac", Context.MODE_PRIVATE);
                    String val = shared.getString("phone", "");
                    if (val.length() == 0) {

                        Intent intent = new Intent(SplashScreen_Activity.this, MainActivity.class);
                        startActivity(intent);
                        finish();


                    } else {

                        Intent intent = new Intent(SplashScreen_Activity.this, Menu_Option_Activity.class);
                        startActivity(intent);
                        finish();
                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        };

        thread.start();
    }

}