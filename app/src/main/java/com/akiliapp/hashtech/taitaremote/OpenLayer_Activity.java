package com.akiliapp.hashtech.taitaremote;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.akiliapp.hashtech.taitaremote.Vehicles.VehicleRecyclerViewAdapter;
import com.akiliapp.hashtech.taitaremote.app.Constants;
import com.akiliapp.hashtech.taitaremote.model.UserResponse;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class OpenLayer_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, PopupMenu.OnMenuItemClickListener {


        WebView webView;
        Button view_vehicles;
        MapView map;

        TextToSpeech t1;

    Timer timer = new Timer();

    JSONObject jsonObject;
    RequestQueue rQueue;

    private static final String IMAGE_DIRECTORY = "/demonuts";
    private int GALLERY = 1, CAMERA = 2;

    public static String userphone = "";

    public final static String BASEURL = "http://148.251.138.82:3000/";
    public final static String URL_USER_LATLONG = BASEURL + "my_vehicle_view";

        private static final int send_email = 0;
        private static final int make_call = 1;
        private static final int send_cloud = 2;
        ImageView dp_image;
        // private VehicleRecyclerViewAdapter adapter;

        EditText phoneText;
        String regno, fullname, password;
        private String LOGIN_URL = "";
        public static String kill_session = "";

        private RecyclerView recyclerView;
        private RecyclerView.Adapter adapter;
        private List<ListItem> listItems;

        @SuppressLint("SetJavaScriptEnabled")
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_open_layer);

            // listItems = new ArrayList<>();
//            Timer timer = new Timer();
//            timer.schedule(new TimerTask() {
//                @Override
//                public void run() {
                 //   getUserLateLong();
//                }
//            }, 5000);

//            dp_image = (ImageView) findViewById(R.id.dp_image);
//            dp_image.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(), "lat: ", Toast.LENGTH_LONG).show();
//                }
//            });

            t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if(status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.US);
                    }
                }
            });

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
//            view_vehicles = (Button) findViewById(R.id.view_vehicles);
//            view_vehicles.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });
            requestMultiplePermissions();
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            NavigationView navigationView = findViewById(R.id.nav_view);
            dp_image = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.dp_image);
            dp_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPictureDialog();
                   // Toast.makeText(getApplicationContext(), "lat: ", Toast.LENGTH_LONG).show();
                }
            });
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
//            toggle.setDrawerIndicatorEnabled(true);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer_menu);
            getSupportActionBar().setTitle("");

            navigationView.setNavigationItemSelectedListener(this);
         //   getUserLateLong();


            regno = getIntent().getStringExtra("EXTRA_SESSION_ID");
//            webView = (WebView) findViewById(R.id.webView);
//            WebSettings webSetting = webView.getSettings();
//            webSetting.setBuiltInZoomControls(true);
//            webSetting.setJavaScriptEnabled(true);
//            webSetting.setLoadWithOverviewMode(true);

            final Context ctx = getApplicationContext();
            Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

            map = (MapView) findViewById(R.id.map);
//            map.setMapRotation(0f);
           // map.setTileSource(TileSourceFactory.OPEN_SEAMAP);
            final IMapController mapController = map.getController();
            mapController.setZoom(19.8);
            map.setBuiltInZoomControls(true);
            map.setMultiTouchControls(true);

            TimerTask t = new TimerTask() {
                @Override
                public void run() {


                    StringRequest request = new StringRequest(Request.Method.POST, URL_USER_LATLONG, new Response.Listener<String>() {

        @Override
        public void onResponse(String response) {


            try {
                JSONObject jsonObject = new JSONObject(response);

                if (jsonObject.has("error") && !jsonObject.optBoolean("error")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");


                    if (jsonArray != null && jsonArray.length() > 0) {
                        int length = jsonArray.length();
                        ArrayList<UserResponse> userResponses = new ArrayList<>();

                        for (int n = 0; n < length; n++) {
                            JSONObject latlongs = jsonArray.getJSONObject(n);
                            if (latlongs.has("lastValidLatitude") && latlongs.has("lastValidLongitude")) {
                                if (latlongs.optString("lastValidLatitude") != null && !latlongs.optString("lastValidLatitude").isEmpty() && !latlongs.optString("lastValidLatitude").equalsIgnoreCase("null") && latlongs.optString("lastValidLongitude") != null && !latlongs.optString("lastValidLongitude").isEmpty() && !latlongs.optString("lastValidLongitude").equalsIgnoreCase("null")) {
                                    UserResponse userResponse = new UserResponse();
                                    userResponse.setLastValidLatitude(latlongs.optString("lastValidLatitude"));
                                    userResponse.setLastValidLongitude(latlongs.optString("lastValidLongitude"));
                                    userResponse.setStatus_lock(latlongs.optString("status_lock"));
                                    userResponse.setVin(latlongs.optString("vin"));
                                    userResponse.setSpeed(latlongs.optString("speed"));
                                    userResponse.setCourse(latlongs.optString("course"));
                                    userResponse.setPlateNumber(latlongs.optString("plate_number"));
                                    userResponses.add(userResponse);
                                    double lat = Double.parseDouble(userResponses.get(n).getLastValidLatitude());
                                    double lon = Double.parseDouble(userResponses.get(n).getLastValidLongitude());
                                    GeoPoint startPoint = new GeoPoint(lat, lon);
                                    Marker startMarker = new Marker(map);
//                                    startMarker.

                                    int x_speed = Integer.parseInt(userResponses.get(n).getSpeed().toString());
                                    if (userResponses.get(n).getVin().equals("1")) {
                                        startMarker.setIcon(ctx.getResources().getDrawable(R.drawable.ic_kid));
                                    }
                                    if (userResponses.get(n).getVin().equals("2") && x_speed == 0) {
                                        startMarker.setIcon(ctx.getResources().getDrawable(R.drawable.ic_park_boda));
                                    }
                                    if (userResponses.get(n).getVin().equals("2") && x_speed > 0) {
                                        startMarker.setIcon(ctx.getResources().getDrawable(R.drawable.ic_trip_boda));
                                    }
                                    if (userResponses.get(n).getVin().equals("3")) {
                                        startMarker.setIcon(ctx.getResources().getDrawable(R.drawable.bajaj_icon_map));
                                    }
                                    if (userResponses.get(n).getVin().equals("4")) {
                                        startMarker.setIcon(ctx.getResources().getDrawable(R.drawable.ic_car_used));
                                    }
                                    startMarker.setDraggable(true);
                                    double angl = Double.parseDouble(userResponses.get(n).getCourse());
                                    float angle = (float) -angl;
                                    startMarker.setRotation(angle);
                                    startMarker.setPosition(startPoint);
                                    map.getOverlayManager().clear();
                                    map.getOverlays().add(startMarker);
//                                    map.getOverlays().remove(this);
                                    map.invalidate();
                                    mapController.setCenter(startPoint);
                                    //Toast.makeText(getApplicationContext(), "lat: " + angle, Toast.LENGTH_LONG).show();
                                }
                            }

                        }

                        userResponseslatlong = userResponses;

                    }


                } else {
                    String message = jsonObject.optString("message");
                    //   Toast.makeText(OpenLayer_Activity.this, "message", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                Log.d("OpenLayer_Activity", e.getMessage());
            }

        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            //   Toast.makeText(getApplicationContext(), "bba " + error, Toast.LENGTH_SHORT).show();
            error.printStackTrace();


        }
    }) {
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            Map<String, String> params = new HashMap<>();
            params.put("userphone", SplashScreen_Activity.sessionId);
//              params.put("userphone", "255656272441");
            //params.put("pass", pass);
            return params;

        }
    };
//     Volley.newRequestQueue(getActivity()).add(request);
//                    queue.add(request);
                        Volley.newRequestQueue(getApplicationContext()).add(request);
//                            request.setShouldCache(false);
//                            queue.getCache().clear();

//    map.;
//}

//request.cancel();
                }

            };
            timer.scheduleAtFixedRate(t,0,5000);


//                }
//            }, 0, 5, TimeUnit.SECONDS);
////
//                    }
//
//                }, 0, 5000);
           // }else{

          //  }
                    //  timer.cancel();

//                    timer.cancel();
//                    timer.purge();
//                }
//            };
//
//            timer.schedule(task, 5000);
           // System.exit(0);

        }

    public void onResume(){
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
//        timer.
//        map.
    }

    public void onPause(){
        super.onPause();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration.getInstance().save(this, prefs);
        map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
        timer.cancel();
        timer.purge();


    }


    public boolean onMenuItemClick(MenuItem item) {
      //  Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.nameDriver:
               // Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
              //  MenuItem name = item.setTitle("ajajaj");
               // Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder mBuilder = new AlertDialog .Builder(OpenLayer_Activity.this);
                View mView = getLayoutInflater().inflate(R.layout.fragment_vehicle, null);
              //  final TextView textView2 = (TextView) mView.findViewById(R.id.textView2);
              //  textView2.setText("baba");

                return true;
            case R.id.two:
                MenuItem name1 = item.setTitle("ajajaj");
                return true;
            case R.id.three:
                // do your code
                return true;
            default:
                return false;
        }
    }


        @Override
        public void onBackPressed() {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }

        @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            // Handle navigation view item clicks here.
            int id = item.getItemId();

//            if(id == R.id.dp_image){
//                Toast.makeText(getApplicationContext(), "askjda", Toast.LENGTH_SHORT).show();
//            }

//            if (id == R.id.nav_myaccount) {
//                Intent newAct = new Intent(this, MyAccountOrginalActivity.class);
//                startActivity(newAct);
//                // Handle the camera action
////            } else if (id == R.id.nav_service) {
////                Intent newSev = new Intent(this, ServiceActivity.class);
////                startActivity(newSev);
//            } else

                if (id == R.id.nav_logout) {
                    SharedPreferences shared = getSharedPreferences("maco", Context.MODE_PRIVATE);
                    String val = shared.getString("reg_no", "");

                        Intent intent = new Intent(OpenLayer_Activity.this, LoginActivity.class);
                        intent.putExtra("EXTRA_SESSION_ID", val);
                        startActivity(intent);
//                        Bundle bundle = new Bundle();
//                        bundle.putString("reg_no", "EXTRA_SESSION_ID");
//                        VehicleFragment myFrag = new VehicleFragment();
//                        myFrag.setArgnts(bundle);
                       // finish();

                   // kill_session = val;


//                    SharedPreferences shared = getSharedPreferences("mac", Context.MODE_PRIVATE);
//                    String val = shared.getString("reg_no", "");
//
//                    SharedPreferences shared = getSharedPreferences("mac", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = shared.edit();
//                    editor.putString("reg_no", LoginActivity.userphone);
//                    editor.commit();
//                    Intent intent = new Intent(OpenLayer_Activity.this, LoginActivity.class);
//                    intent.putExtra("EXTRA_SESSION_ID", val);
//                    startActivity(intent);
//                    finish();


            }

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

    /**
     * Called when pointer capture is enabled or disabled for the current window.
     *
     * @param hasCapture True if the window has pointer capture.
     */
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }


        private class WebViewClient extends android.webkit.WebViewClient {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        }


        ArrayList<UserResponse> userResponseslatlong = new ArrayList<>();

        public void getUserLateLong() {
            StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_USER_LATLONG,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.has("error") && !jsonObject.optBoolean("error")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    if (jsonArray != null && jsonArray.length() > 0) {
                                        int length = jsonArray.length();
                                        ArrayList<UserResponse> userResponses = new ArrayList<>();

                                        for (int n = 0; n < length; n++) {
                                            JSONObject latlongs = jsonArray.getJSONObject(n);
                                            if (latlongs.has("lastValidLatitude") && latlongs.has("lastValidLongitude")) {
                                                if (latlongs.optString("lastValidLatitude") != null && !latlongs.optString("lastValidLatitude").isEmpty() && !latlongs.optString("lastValidLatitude").equalsIgnoreCase("null")
                                                        && latlongs.optString("lastValidLongitude") != null && !latlongs.optString("lastValidLongitude").isEmpty() && !latlongs.optString("lastValidLongitude").equalsIgnoreCase("null")) {
                                                    UserResponse userResponse = new UserResponse();
                                                    userResponse.setLastValidLatitude(latlongs.optString("lastValidLatitude"));
                                                    userResponse.setLastValidLongitude(latlongs.optString("lastValidLongitude"));
                                                    userResponse.setStatus_lock("status_lock");
                                                    userResponse.setVin("vin");
                                                    userResponse.setAngle("course");
                                                    userResponses.add(userResponse);
                                                }
                                            }

                                        }
                                        userResponseslatlong = userResponses;
//                                        userResponseslatlong.
                                        //createNewLayerpoints();
                                        loadMap();
                                    }

                                } else {
                                    String message = jsonObject.optString("message");
                                 //   Toast.makeText(OpenLayer_Activity.this, "message", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                Log.d("OpenLayer_Activity", e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //   Toast.makeText(getApplicationContext(), "bba " + error, Toast.LENGTH_SHORT).show();
                    error.printStackTrace();


                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("userphone", SplashScreen_Activity.sessionId);
                    //params.put("pass", pass);
                    return params;
                }
            };
            // Volley.newRequestQueue(getActivity()).add(request);
            Volley.newRequestQueue(getApplicationContext()).add(request);
        }


        public void loadMap() {

            //if(userResponseslatlong.get())

            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                   // createNewLayerpoints();
                    StringBuilder newLatLong = new StringBuilder();
                    if (userResponseslatlong != null && userResponseslatlong.size() > 0) {

                        newLatLong.append("javascript:(function() {");
                        newLatLong.append("var size = new OpenLayers.Size(28, 56);");
                        newLatLong.append("var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);");
//                        if(userResponseslatlong.get)

                        for (int n = 0; n < userResponseslatlong.size(); n++) {
                                  //  if(userResponseslatlong.get(n).geta)
                            newLatLong.append("      " +
                                    "var newIcon = new OpenLayers.Icon('http://akiliapp.co.tz/bodaboda_icon/bodaboda_2.png', size, offset);");

                            newLatLong.append("var lonLat" + n + " = new OpenLayers.LonLat(" + userResponseslatlong.get(n).getLastValidLongitude() + "," + userResponseslatlong.get(n).getLastValidLatitude() + ")");
                            newLatLong.append(".transform(\n" +
                                    "                  new OpenLayers.Projection(\"EPSG:4326\"), \n" +
                                    "                  map.getProjectionObject()\n" +
                                    "                );\n" +
                                    " var zoom = 12;");
                            newLatLong.append("markers.addMarker(new OpenLayers.Marker(lonLat" + n + ",newIcon.clone()));\n");
                        }
                        newLatLong.append("})()");
                        webView.loadUrl(newLatLong.toString());
                    }
                }
            });
            webView.loadUrl("file:///android_asset/index.php");

        }




        ArrayList<Result_Node> resultList1 = new ArrayList<>();

        public void SingleDeviceMethod() {

//                Timer t = new Timer();
//                t.schedule(new TimerTask() {
//                    @Override
//                    public void run() {

            StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_SINGLE_DEVICE,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray jsonArray = jsonObject.getJSONArray("result");

                                ArrayList<Result_Node> resultList = new ArrayList<>();

                                // for(int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(0);

                                String lastValidLatitude = jsonObject1.getString("lastValidLatitude");
                                String lastValidLongitude = jsonObject1.getString("lastValidLongitude");
                                String angle = jsonObject1.getString("angle");
                                String vin = jsonObject1.getString("vin");
                                String status_lock = jsonObject1.getString("status_lock");


                                 // Toast.makeText(getApplicationContext(), angle , Toast.LENGTH_SHORT).show();

                                Result_Node result = new Result_Node();
                                result.setLastValidLatitude(lastValidLatitude);
                                result.setLastValidLongitude(lastValidLongitude);
                                result.setAngle(angle);
                                result.setVin(vin);
                                result.setStatus_lock(status_lock);
                                //  Toast.makeText(getApplicationContext(), "dds " + status_lock,Toast.LENGTH_SHORT).show();
                                resultList.add(result);

                                resultList1 = resultList;
//                                new java.util.Timer().schedule(
//                    new java.util.TimerTask() {
//                        @Override
//                       public void run() {
                                loadMapSingleDevice();
//                        }
//                    }, 0, 5000);


                                // }
                                // recyclerView.setAdapter(new VehicleRecyclerViewAdapter(getContext(), resultList, mListener));

                            } catch (JSONException e) {
                               // Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_SHORT).show();
                                //  Toast.makeText(getContext(), ""+e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                 //   Toast.makeText(getApplicationContext(), "bba " + error, Toast.LENGTH_SHORT).show();
                    error.printStackTrace();


                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("uniqueId", VehicleRecyclerViewAdapter.imei);
                    //params.put("pass", pass);
                    return params;
                }
            };
            // Volley.newRequestQueue(getActivity()).add(request);
            Volley.newRequestQueue(getApplicationContext()).add(request);

//            }
//        }, 0, 10000);

        }

        public void loadMapSingleDevice() {
           // SingleDeviceMethod();
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                }
            });
            int a = Integer.parseInt(resultList1.get(0).getAngle());
            int zoom = 18;
            if(resultList1.get(0).getVin().equals("4") && resultList1.get(0).getStatus_lock().equals("1")){
                //our map openlayer from OSM

                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
                        "var size = new OpenLayers.Size(28, 56);" +
                        "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        //"var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/vehicle_rotation/"+a+".png');" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);

            }  if(resultList1.get(0).getVin().equals("4") && resultList1.get(0).getStatus_lock().equals("0")){

                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
//                            "var size = new OpenLayers.Size(71.55, 118.41);" +
//                            "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        // "var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        //  "var icon = new OpenLayers.Icon('http://akiliapp.co.tz/bodaboda_icon/bodaboda_2.png', size, offset);" +
                        // "var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on_left.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/vehicle_rotation/"+a+".png');" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);

            }
            else if(resultList1.get(0).getVin().equals("2") && resultList1.get(0).getStatus_lock().equals("1")){
                //our map openlayer from OSM
                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
                        "var size = new OpenLayers.Size(30, 60);" +
                        "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        //"var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/motorcycle_angle_active/"+a+".png', size, offset);" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);
            }  else if(resultList1.get(0).getVin().equals("2") && resultList1.get(0).getStatus_lock().equals("0")){
                //our map openlayer from OSM
                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
                        "var size = new OpenLayers.Size(30, 60);" +
                        "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        //"var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/motorcycle_angle_active/"+a+".png');" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);
            }
            else if(resultList1.get(0).getVin().equals("3") && resultList1.get(0).getStatus_lock().equals("1")){
                //our map openlayer from OSM
                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
                        "var size = new OpenLayers.Size(30, 60);" +
                        "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        //"var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/bajaj_angle_active/"+a+".png', size, offset);" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);
            }  else if(resultList1.get(0).getVin().equals("3") && resultList1.get(0).getStatus_lock().equals("0")){
                //our map openlayer from OSM
                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
                        "var size = new OpenLayers.Size(30, 60);" +
                        "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        //"var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/bajaj_angle_active/"+a+".png');" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);
            }
        }

        //request permissions for camera n gallery

    private void  requestMultiplePermissions(){
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(getApplicationContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(List&lt;PermissionRequest&gt; permissions, PermissionToken token) {
//                        token.continuePermissionRequest();
//                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    //here is where to upload profile to db
                    uploadImage(bitmap);
                   //Z Toast.makeText(OpenLayer_Activity.this, "Image Saved Jamal!", Toast.LENGTH_SHORT).show();
                    dp_image.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(OpenLayer_Activity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            dp_image.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            Toast.makeText(OpenLayer_Activity.this, "Image Saved!" + thumbnail, Toast.LENGTH_SHORT).show();
        }
    }
    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);

        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + f.getAbsolutePath());
            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }
    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }



    private void uploadImage(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
        try {
            jsonObject = new JSONObject();
            String imgname = String.valueOf(Calendar.getInstance().getTimeInMillis());
            jsonObject.put("name", imgname);
            //  Log.e("Image name", etxtUpload.getText().toString().trim());
            jsonObject.put("image", encodedImage);
            // jsonObject.put("aa", "aa");
            //Toast.makeText(getApplication(), "Image " +encodedImage, Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            Log.e("JSONObject Here", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://148.251.138.82:3000/geocode_location_new", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("aaaaaaa", jsonObject.toString());
                        rQueue.getCache().clear();
                        Toast.makeText(getApplication(), "Image Uploaded Successfully", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("aaaaaaa", volleyError.toString());
            }
        });
        rQueue = Volley.newRequestQueue(OpenLayer_Activity.this);
        rQueue.add(jsonObjectRequest);
    }

    }


