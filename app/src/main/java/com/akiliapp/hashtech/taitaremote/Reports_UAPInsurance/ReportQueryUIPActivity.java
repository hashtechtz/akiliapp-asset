package com.akiliapp.hashtech.taitaremote.Reports_UAPInsurance;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.akiliapp.hashtech.taitaremote.R;

import androidx.appcompat.app.AppCompatActivity;

public class ReportQueryUIPActivity extends AppCompatActivity {

    Button querybtn;
    TextView progress_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportsuap_accident);

        querybtn = (Button) findViewById(R.id.querybtn);
        querybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Request successfully sent", Toast.LENGTH_SHORT).show();
            }
        });
        progress_view = (TextView) findViewById(R.id.progress_view);
        progress_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "You dont have any reported query up to now", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
