package com.akiliapp.hashtech.taitaremote.Payments;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.akiliapp.hashtech.taitaremote.MyAccountOrginalActivity;
import com.akiliapp.hashtech.taitaremote.Payments.dialog.AndroidLDialog;
import com.akiliapp.hashtech.taitaremote.R;

public class PaymentOneWeekActivity extends AppCompatActivity {

    Button buttonPayOneWeek;
    EditText editText2;
    AndroidLDialog androidLDialog;
    Activity mActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_oneweek);



        buttonPayOneWeek = (Button) findViewById(R.id.buttonPayOneWeek);
        editText2 = (EditText) findViewById(R.id.editText2);
        buttonPayOneWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   insertData();
                dialogExample();

                }
            });
//           dialogExample();




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
//      ab.setIcon(R.drawable.ic_boda_icon);
        ab.setTitle("Thibitisha Malipo");


    }

//    private void insertData() {
//      //  String amount=editText2.getText().toString().trim();
//        String user="1259706349";
//
//
//        Call<ResponseBody> call=MyClient.getInstance().getMyApi().insertdata(user);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
// //               Toast.makeText(getApplicationContext(), "Ombi lako linashughulikiwa. Utapokea ujumbe wa uthibitisho.", Toast.LENGTH_SHORT).show();
//
// //               startActivity(new Intent(getApplicationContext(), DialogConfirm_Activity.class));
//
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Ombi lako halijafanikiwa. Kuna tatizo la kiufundi tafadhali jaribu tena baadae.", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(getApplicationContext(), ChoosePaymentActivity.class));
//            }
//        });
//    }


    private void dialogExample() {
        androidLDialog = new AndroidLDialog.Builder(PaymentOneWeekActivity.this)

                //settings title
                .Title("TigoPesa")
                //settings message
                .Message("1. Tuma Pesa\n2. Vocha na Vifurushi\n3. Kutoa Pesa\n4. Lipa Bili\n5. Lipa Bidhaa 6. Jihudumie\n" +
                        "7. Huduma za kifedha.\n8. Language")
                //adding positive (right) button
                .setPositiveButton("Sawa",new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


//                                    if (v == buttonPayOneWeek)
//                                        androidLDialog.dismiss();
//                                    else {
//                                        Intent i = new Intent(mActivity, ChoosePaymentActivity.class);
//                                        mActivity.startActivity(i);
//                                    }




                                Intent intent = new Intent(PaymentOneWeekActivity.this, ChoosePaymentActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

//                        Toast.makeText(PaymentOneWeekActivity.this, "You clicked OK", Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(getApplicationContext(), ChoosePaymentActivity.class));
                                //androidLDialog.hide();

//                                if (v == buttonPayOneWeek)
//                                    androidLDialog.dismiss();
//                                else {
////                                    Intent i = new Intent(String.valueOf(ChoosePaymentActivity.class));
////                                    mActivity.startActivity(i);
//                                    Intent i = new Intent(getBaseContext(), ChoosePaymentActivity.class);
//                                    startActivity(i);
//                                }

                            }
                        })


//                //adding negative (center) button
//                .setNegativeButton("CANCEL", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Toast.makeText(DialogGroupRegistering.this, "You clicked CANCEL", Toast.LENGTH_SHORT).show();
//                        androidLDialog.hide();
//                    }
//                })


                //showing the dialog!
                .show();

        //optional.. setting icon but it's not very android l like
        //androidLDialog.setIcon(R.drawable.ic_launcher);
        /**
         * Try out functions like
         *         androidLDialog.getTitle();
         *         androidLDialog.setMessageTextSize(CUSTOM_SIZE);
         *         androidLDialog.setTitleTextSize(CUSTOM_SIZE);
         *         androidLDialog.getMessage();
         *         androidLDialog.setMessageColor(android.R.color.black);
         *         androidLDialog.setTitleColor(android.R.color.black);
         *         androidLDialog.setBackground(R.drawable.someBackground);
         *
         *         SEE ALL FEATURES OF THIS LIBRARY IN THE README
         */

        //       androidLDialog.setTitleColor(android.R.color.white);


    }

}




