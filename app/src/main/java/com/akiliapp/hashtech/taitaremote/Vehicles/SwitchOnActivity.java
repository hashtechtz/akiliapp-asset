package com.akiliapp.hashtech.taitaremote.Vehicles;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.akiliapp.hashtech.taitaremote.OpenLayer_Activity;
import com.akiliapp.hashtech.taitaremote.R;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class SwitchOnActivity extends AppCompatActivity {

    Button button;
    String URL_SWITCH = "https://akiliapp.com/api/send_gprs_command?user_api_hash=$2y$10$JqOIAN1H5UsK5/2VHXx3te80M4KvXWoVe/qeu0bD.KEGKban5ZtuW&lang=en&device_id=95&type=engineResume";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_on);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cofirmationCodeSend();

              //  startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }
    public void cofirmationCodeSend(){
        //  Toast.makeText(Menu_Option_Activity.this, userid, Toast.LENGTH_SHORT).show();
        StringRequest request = new StringRequest(Request.Method.POST, URL_SWITCH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), "Kifaa kimewashwa kikamilifu.", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), OpenLayer_Activity.class));
                        //   System.out.println(response);

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Kuna tatizo kifaa chako hakijapokea amri.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), OpenLayer_Activity.class));
                //error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // params.put("userid", "215");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }
}
