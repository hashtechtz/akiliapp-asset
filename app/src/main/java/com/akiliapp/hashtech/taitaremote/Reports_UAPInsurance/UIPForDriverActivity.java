package com.akiliapp.hashtech.taitaremote.Reports_UAPInsurance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.akiliapp.hashtech.taitaremote.R;


public class UIPForDriverActivity extends AppCompatActivity {

    Button querybtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uipfor_driver);

        querybtn = (Button) findViewById(R.id.querybtn);
        querybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ReportQueryUIPActivity.class));
            }
        });
    }
}
