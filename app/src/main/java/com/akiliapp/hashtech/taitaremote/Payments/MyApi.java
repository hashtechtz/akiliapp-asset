package com.akiliapp.hashtech.taitaremote.Payments;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface MyApi {
    @FormUrlEncoded
    @POST("oneweekpayment.php")
    Call<ResponseBody> insertdata(
                                  @Field("user") String user

    );
}




