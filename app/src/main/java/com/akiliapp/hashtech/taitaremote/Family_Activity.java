package com.akiliapp.hashtech.taitaremote;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.akiliapp.hashtech.taitaremote.Vehicles.VehicleRecyclerViewAdapter;
import com.akiliapp.hashtech.taitaremote.app.Constants;
import com.akiliapp.hashtech.taitaremote.model.UserResponse;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
//import org.osmdroid.api.IMapController;
//import org.osmdroid.config.Configuration;
//import org.osmdroid.util.GeoPoint;
//import org.osmdroid.views.MapView;
//import org.osmdroid.views.overlay.Marker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Family_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, PopupMenu.OnMenuItemClickListener {


        WebView webView;
        Button view_vehicles;
      //  MapView map;

        private static final int send_email = 0;
        private static final int make_call = 1;
        private static final int send_cloud = 2;
        // private VehicleRecyclerViewAdapter adapter;

        EditText phoneText;
        String regno, fullname, password;
        private String LOGIN_URL = "";
        public static String kill_session = "";

        private RecyclerView recyclerView;
        private RecyclerView.Adapter adapter;
        private List<ListItem> listItems;

        @SuppressLint("SetJavaScriptEnabled")
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_family);

            // listItems = new ArrayList<>();
//            Timer timer = new Timer();
//            timer.schedule(new TimerTask() {
//                @Override
//                public void run() {
                //    getUserLateLong();
//                }
//            }, 5000);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
//            view_vehicles = (Button) findViewById(R.id.view_vehicles);
//            view_vehicles.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    PopupMenu popupMenu = new PopupMenu(OpenLayer_Activity.this, v);
//                    popupMenu.setOnMenuItemClickListener(OpenLayer_Activity.this);
//                    popupMenu.inflate(R.menu.popup_menu);
//                  //  popupMenu.getMenu().getItem()
//                   // popupMenu.getMenu().add(0, 0, 0, getText(R.string.action_settings));
//                   // popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());
//                  //  popupMenu.
//                    popupMenu.show();

//                    AlertDialog.Builder mBuilder = new AlertDialog .Builder(OpenLayer_Activity.this);
//                    View mView = getLayoutInflater().inflate(R.layout.list_vehicles, null);
//                    final TextView textView2 = (TextView) mView.findViewById(R.id.textView2);
//                    //final TextView uniqueid = (TextView) mView.findViewById(R.id.uniqueid);
//                    textView2.setText("list ya piipiki");
//                    mBuilder.setView(mView);
//                    AlertDialog dialog = mBuilder.create();
//                    dialog.show();

//                }
//            });
//            toolbar.setTitle("");

//        ActionBar ab = getActionBar();
//        ab.setTitle(" ");
//        ab.setSubtitle(getResources().getString(R.string.mySubTitle));

//        topToolBar.setLogo(R.drawable.ic_moving);
//        topToolBar.setLogoDescription(getResources().getString(R.string.logo_desc));
//        listItems = new ArrayList<>();


//    DRAWER START
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            NavigationView navigationView = findViewById(R.id.nav_view);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            toggle.setDrawerIndicatorEnabled(true);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer_menu);
            getSupportActionBar().setTitle("");
//
            navigationView.setNavigationItemSelectedListener(this);
         //   getUserLateLong();

//            final Context ctx = getApplicationContext();
//            Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

            regno = getIntent().getStringExtra("EXTRA_SESSION_ID");
//            webView = (WebView) findViewById(R.id.webView);
//            WebSettings webSetting = webView.getSettings();
//            webSetting.setBuiltInZoomControls(true);
//            webSetting.setJavaScriptEnabled(true);
//            webSetting.setLoadWithOverviewMode(true);

//            final IMapController mapController = map.getController();
//            mapController.setZoom(18);
//
//
//
////        getUserLateLong();
//            // map.setTileSource(TileSourceFactory.MAPNIK);
//
//            map.setBuiltInZoomControls(true);
//            map.setMultiTouchControls(true);
//
//
////            double lat = Double.parseDouble(resultList1.get(0).lastValidLatitude);
////            double lon = Double.parseDouble(resultList1.get(0).lastValidLongitude);
//            GeoPoint startPoint = new GeoPoint(-6.765222222222, 37.896523244);
//            Marker startMarker = new Marker(map);
//            startMarker.setIcon(ctx.getResources().getDrawable(R.drawable.ic_kid));
//            int given_angle = Integer.parseInt(resultList1.get(0).getAngle());
//            float angl = given_angle;
//
//            startMarker.setInfoWindow(null);
//
//            startMarker.setTitle("Mahali ilipo:\nMwendo:\nSiku zilizobaki kulipia: 5");
//            startMarker.setPosition(startPoint);
//            startMarker.setRotation(angl);
//            map.getOverlays().add(startMarker);
//            mapController.setCenter(startPoint);

          //  webView.loadUrl("file:///android_asset/www/openlayer.html");


//            View bottomSheet = findViewById(R.id.bottomSheet);
//            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
//            bottomSheetBehavior.setPeekHeight(400);


            // getVehicles("1259704999");
            //   Toast.makeText(getApplicationContext(), regno,Toast.LENGTH_SHORT).show();

        }


    public boolean onMenuItemClick(MenuItem item) {
      //  Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.nameDriver:
               // Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
              //  MenuItem name = item.setTitle("ajajaj");
               // Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
                AlertDialog.Builder mBuilder = new AlertDialog .Builder(Family_Activity.this);
                View mView = getLayoutInflater().inflate(R.layout.fragment_vehicle, null);
              //  final TextView textView2 = (TextView) mView.findViewById(R.id.textView2);
              //  textView2.setText("baba");

                return true;
            case R.id.two:
                MenuItem name1 = item.setTitle("ajajaj");
                return true;
            case R.id.three:
                // do your code
                return true;
            default:
                return false;
        }
    }


        @Override
        public void onBackPressed() {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }

        @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            // Handle navigation view item clicks here.
            int id = item.getItemId();

//            if (id == R.id.nav_myaccount) {
//                Intent newAct = new Intent(this, MyAccountOrginalActivity.class);
//                startActivity(newAct);
//                // Handle the camera action
////            } else if (id == R.id.nav_service) {
////                Intent newSev = new Intent(this, ServiceActivity.class);
////                startActivity(newSev);
//            } else

                if (id == R.id.nav_logout) {
                    SharedPreferences shared = getSharedPreferences("maco", Context.MODE_PRIVATE);
                    String val = shared.getString("reg_no", "");

                        Intent intent = new Intent(Family_Activity.this, LoginActivity.class);
                        intent.putExtra("EXTRA_SESSION_ID", val);
                        startActivity(intent);
//                        Bundle bundle = new Bundle();
//                        bundle.putString("reg_no", "EXTRA_SESSION_ID");
//                        VehicleFragment myFrag = new VehicleFragment();
//                        myFrag.setArguments(bundle);
                       // finish();

                   // kill_session = val;


//                    SharedPreferences shared = getSharedPreferences("mac", Context.MODE_PRIVATE);
//                    String val = shared.getString("reg_no", "");
//
//                    SharedPreferences shared = getSharedPreferences("mac", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = shared.edit();
//                    editor.putString("reg_no", LoginActivity.userphone);
//                    editor.commit();
//                    Intent intent = new Intent(OpenLayer_Activity.this, LoginActivity.class);
//                    intent.putExtra("EXTRA_SESSION_ID", val);
//                    startActivity(intent);
//                    finish();


            }

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

    /**
     * Called when pointer capture is enabled or disabled for the current window.
     *
     * @param hasCapture True if the window has pointer capture.
     */
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }


        private class WebViewClient extends android.webkit.WebViewClient {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        }


        ArrayList<UserResponse> userResponseslatlong = new ArrayList<>();

        public void getUserLateLong() {
            StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_USER_LATLONG,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.has("error") && !jsonObject.optBoolean("error")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    if (jsonArray != null && jsonArray.length() > 0) {
                                        int length = jsonArray.length();
                                        ArrayList<UserResponse> userResponses = new ArrayList<>();

                                        for (int n = 0; n < length; n++) {
                                            JSONObject latlongs = jsonArray.getJSONObject(n);
                                            if (latlongs.has("lastValidLatitude") && latlongs.has("lastValidLongitude")) {
                                                if (latlongs.optString("lastValidLatitude") != null && !latlongs.optString("lastValidLatitude").isEmpty() && !latlongs.optString("lastValidLatitude").equalsIgnoreCase("null")
                                                        && latlongs.optString("lastValidLongitude") != null && !latlongs.optString("lastValidLongitude").isEmpty() && !latlongs.optString("lastValidLongitude").equalsIgnoreCase("null")) {
                                                    UserResponse userResponse = new UserResponse();
                                                    userResponse.setLastValidLatitude(latlongs.optString("lastValidLatitude"));
                                                    userResponse.setLastValidLongitude(latlongs.optString("lastValidLongitude"));
                                                    userResponse.setStatus_lock("status_lock");
                                                    userResponse.setVin("vin");
                                                    userResponse.setAngle("course");
                                                    userResponses.add(userResponse);
                                                }
                                            }

                                        }
                                        userResponseslatlong = userResponses;
//                                        userResponseslatlong.
                                        //createNewLayerpoints();
                                        loadMap();
                                    }

                                } else {
                                    String message = jsonObject.optString("message");
                                 //   Toast.makeText(OpenLayer_Activity.this, "message", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                Log.d("OpenLayer_Activity", e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //   Toast.makeText(getApplicationContext(), "bba " + error, Toast.LENGTH_SHORT).show();
                    error.printStackTrace();


                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("userphone", SplashScreen_Activity.sessionId);
                    //params.put("pass", pass);
                    return params;
                }
            };
            // Volley.newRequestQueue(getActivity()).add(request);
            Volley.newRequestQueue(getApplicationContext()).add(request);
        }


        public void loadMap() {

            //if(userResponseslatlong.get())

            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                   // createNewLayerpoints();
                    StringBuilder newLatLong = new StringBuilder();
                    if (userResponseslatlong != null && userResponseslatlong.size() > 0) {

                        newLatLong.append("javascript:(function() {");
                        newLatLong.append("var size = new OpenLayers.Size(28, 56);");
                        newLatLong.append("var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);");
//                        if(userResponseslatlong.get)

                        for (int n = 0; n < userResponseslatlong.size(); n++) {
                                  //  if(userResponseslatlong.get(n).geta)
                            newLatLong.append("      " +
                                    "var newIcon = new OpenLayers.Icon('http://akiliapp.co.tz/bodaboda_icon/bodaboda_2.png', size, offset);");

                            newLatLong.append("var lonLat" + n + " = new OpenLayers.LonLat(" + userResponseslatlong.get(n).getLastValidLongitude() + "," + userResponseslatlong.get(n).getLastValidLatitude() + ")");
                            newLatLong.append(".transform(\n" +
                                    "                  new OpenLayers.Projection(\"EPSG:4326\"), \n" +
                                    "                  map.getProjectionObject()\n" +
                                    "                );\n" +
                                    " var zoom = 12;");
                            newLatLong.append("markers.addMarker(new OpenLayers.Marker(lonLat" + n + ",newIcon.clone()));\n");
                        }
                        newLatLong.append("})()");
                        webView.loadUrl(newLatLong.toString());
                    }
                }
            });
            webView.loadUrl("file:///android_asset/index.php");

        }




        ArrayList<Result_Node> resultList1 = new ArrayList<>();

        public void SingleDeviceMethod() {

//                Timer t = new Timer();
//                t.schedule(new TimerTask() {
//                    @Override
//                    public void run() {

            StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_SINGLE_DEVICE,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray jsonArray = jsonObject.getJSONArray("result");

                                ArrayList<Result_Node> resultList = new ArrayList<>();

                                // for(int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(0);

                                String lastValidLatitude = jsonObject1.getString("lastValidLatitude");
                                String lastValidLongitude = jsonObject1.getString("lastValidLongitude");
                                String angle = jsonObject1.getString("angle");
                                String vin = jsonObject1.getString("vin");
                                String status_lock = jsonObject1.getString("status_lock");


                                 // Toast.makeText(getApplicationContext(), angle , Toast.LENGTH_SHORT).show();

                                Result_Node result = new Result_Node();
                                result.setLastValidLatitude(lastValidLatitude);
                                result.setLastValidLongitude(lastValidLongitude);
                                result.setAngle(angle);
                                result.setVin(vin);
                                result.setStatus_lock(status_lock);
                                //  Toast.makeText(getApplicationContext(), "dds " + status_lock,Toast.LENGTH_SHORT).show();
                                resultList.add(result);

                                resultList1 = resultList;
//                                new java.util.Timer().schedule(
//                    new java.util.TimerTask() {
//                        @Override
//                       public void run() {
                                loadMapSingleDevice();
//                        }
//                    }, 0, 5000);


                                // }
                                // recyclerView.setAdapter(new VehicleRecyclerViewAdapter(getContext(), resultList, mListener));

                            } catch (JSONException e) {
                               // Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_SHORT).show();
                                //  Toast.makeText(getContext(), ""+e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                 //   Toast.makeText(getApplicationContext(), "bba " + error, Toast.LENGTH_SHORT).show();
                    error.printStackTrace();


                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("uniqueId", VehicleRecyclerViewAdapter.imei);
                    //params.put("pass", pass);
                    return params;
                }
            };
            // Volley.newRequestQueue(getActivity()).add(request);
            Volley.newRequestQueue(getApplicationContext()).add(request);

//            }
//        }, 0, 10000);

        }

        public void loadMapSingleDevice() {
           // SingleDeviceMethod();
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                }
            });
            int a = Integer.parseInt(resultList1.get(0).getAngle());
            int zoom = 18;
            if(resultList1.get(0).getVin().equals("4") && resultList1.get(0).getStatus_lock().equals("1")){
                //our map openlayer from OSM

                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
                        "var size = new OpenLayers.Size(28, 56);" +
                        "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        //"var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/vehicle_rotation/"+a+".png');" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);

            }  if(resultList1.get(0).getVin().equals("4") && resultList1.get(0).getStatus_lock().equals("0")){

                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
//                            "var size = new OpenLayers.Size(71.55, 118.41);" +
//                            "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        // "var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        //  "var icon = new OpenLayers.Icon('http://akiliapp.co.tz/bodaboda_icon/bodaboda_2.png', size, offset);" +
                        // "var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on_left.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/vehicle_rotation/"+a+".png');" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);

            }
            else if(resultList1.get(0).getVin().equals("2") && resultList1.get(0).getStatus_lock().equals("1")){
                //our map openlayer from OSM
                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
                        "var size = new OpenLayers.Size(30, 60);" +
                        "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        //"var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/motorcycle_angle_active/"+a+".png', size, offset);" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);
            }  else if(resultList1.get(0).getVin().equals("2") && resultList1.get(0).getStatus_lock().equals("0")){
                //our map openlayer from OSM
                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
                        "var size = new OpenLayers.Size(30, 60);" +
                        "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        //"var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/motorcycle_angle_active/"+a+".png');" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);
            }
            else if(resultList1.get(0).getVin().equals("3") && resultList1.get(0).getStatus_lock().equals("1")){
                //our map openlayer from OSM
                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
                        "var size = new OpenLayers.Size(30, 60);" +
                        "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        //"var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/bajaj_angle_active/"+a+".png', size, offset);" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);
            }  else if(resultList1.get(0).getVin().equals("3") && resultList1.get(0).getStatus_lock().equals("0")){
                //our map openlayer from OSM
                String MapSingleDevice = "<html><body>" +
                        "<div id=\"mapdiv\"></div> " +
                        "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>" +
                        "<script> " +
                        "map = new OpenLayers.Map(\"mapdiv\"); " +
                        "map.addLayer(new OpenLayers.Layer.OSM());" +
                        "var lonLat = new OpenLayers.LonLat( " + resultList1.get(0).getLastValidLongitude() + " , " + resultList1.get(0).getLastValidLatitude() + ")" +
                        ".transform(new OpenLayers.Projection(\"EPSG:4326\")," +
                        "map.getProjectionObject());" +
                        "var zoom = "+zoom+";" +
                        "var markers = new OpenLayers.Layer.Markers( \"Markers\" );" +
                        "map.addLayer(markers);" +
                        "var size = new OpenLayers.Size(30, 60);" +
                        "var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"+
                        // "var icon = new OpenLayers.Icon('<img src='"+R.drawable.ic_switch_off_pink+"'>');" +
                        //"var icon = new OpenLayers.Icon('http://akiliapp.automechsaccos.co.tz/car_on.png', size, offset);" +
                        "var icon = new OpenLayers.Icon('http://akilisec.automechsaccos.co.tz/bajaj_angle_active/"+a+".png');" +
                        // "transform: rotate(20deg);" +
                        "markers.addMarker(new OpenLayers.Marker(lonLat,icon));" +
                        "map.setCenter (lonLat, zoom);" +
                        // "markers.bindPopup('kakakaka')" +
                        " </script>" +
                        "</body></html>";
                webView.loadDataWithBaseURL(null, MapSingleDevice, "text/html", "UTF-8", null);
            }
        }
    }


