package com.akiliapp.hashtech.taitaremote;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;


public interface Vehicle_details_API {
    @FormUrlEncoded
    @GET("/vehicle_details")
    Call<Vehicledetails_POJO> insertData(@Field("name") String name,
                                         @Field("uniqueId") String uniqueId,
                                         @Field("plate_number") String plate_number,
                                         @Field("device_model") String device_model,
                                         @Field("lastValidLatitude") String lastValidLatitude,
                                         @Field("lastValidLongitude") String lastValidLongitude,
                                         @Field("registration_number") String registration_number);
}
