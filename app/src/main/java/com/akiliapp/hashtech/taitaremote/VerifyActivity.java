package com.akiliapp.hashtech.taitaremote;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class VerifyActivity extends AppCompatActivity {

    private CountDownTimer cdt;
    private TextView textView14;
    private EditText editText;
    private long timeremain = 30000; //30 sec in ms.
    private boolean timerunning;
    private TextWatcher text = null;
    private TextView textView10,reg_no;

    String fullname, regno, password;
    ///int password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

       // textView14 = (TextView) findViewById(R.id.textView14);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            regno = extras.getString("reg_no");
            fullname = extras.getString("fullname");
            password = extras.getString("password");
        }

        textView10 = (TextView) findViewById(R.id.textView10);
        textView10.setText("Welcome " + fullname);

        reg_no = (TextView) findViewById(R.id.textView12);
        reg_no.setText("ID: " + regno);


        editText = (EditText) findViewById(R.id.editText);

        text = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                int length = String.valueOf(password).length();
                if(editText.getText().toString().length() == length) {
                    if(password.equals(editText.getText().toString())) {
                         editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(length)});
                        //Toast.makeText(getApplicationContext(), "" + length, Toast.LENGTH_SHORT).show();
                        verifiedUser();
                    }else{
                        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(length)});
                        Toast.makeText(getApplicationContext(), "Verification code invalid", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
editText.addTextChangedListener(text);



    }

    //select * vehicles owned by this person
    private void verifiedUser(){
        SharedPreferences shared = getSharedPreferences("Mac", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString("reg_no",regno);
        editor.commit();
     //   Toast.makeText(getApplicationContext(), "yyy" + regno, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), SplashScreen_Activity.class);
        intent.putExtra("reg_no",regno);
        startActivity(intent);
    }
}
