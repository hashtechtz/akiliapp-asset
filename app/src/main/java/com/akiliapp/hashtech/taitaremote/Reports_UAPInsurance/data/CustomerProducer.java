package com.akiliapp.hashtech.taitaremote.Reports_UAPInsurance.data;


/**
 * Data object representing a car producer.
 *
 * @author ISchwarz
 */
public class CustomerProducer {

    private final String name;
//    private final int logoRes;

    public CustomerProducer(final String name) {
//        this.logoRes = logoRes;
        this.name = name;
    }

//    public int getLogo() {
//        return logoRes;
//    }

    public String getName() {
        return name;
    }
}
