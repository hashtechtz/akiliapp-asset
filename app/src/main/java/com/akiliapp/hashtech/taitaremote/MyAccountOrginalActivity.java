package com.akiliapp.hashtech.taitaremote;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.akiliapp.hashtech.taitaremote.Payments.ChoosePaymentActivity;
import com.akiliapp.hashtech.taitaremote.Reports_NHIF.NHIFReportActivity;
import com.akiliapp.hashtech.taitaremote.Reports_NSSF.NSSFReportActivity;
import com.akiliapp.hashtech.taitaremote.Reports_UAPInsurance.UAPReportActivity;

public class MyAccountOrginalActivity extends AppCompatActivity {

    ImageView payment, uap, nssf, nhif, akiliapp, offer;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myaccount_orginal);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
//      ab.setIcon(R.drawable.ic_boda_icon);
        ab.setTitle("Chagua Huduma");


        payment = (ImageView) findViewById(R.id.payments);
        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getApplicationContext(), "sdd", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), ChoosePaymentActivity.class));
            }
        });

        offer = (ImageView) findViewById(R.id.offer);
        offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Hauna ofa mpya kwa sasa...endelea kutumia akiliapp", Toast.LENGTH_SHORT).show();
                // startActivity(new Intent(getApplicationContext(), MapsActivity.class));
            }
        });

        nssf = (ImageView) findViewById(R.id.nssf);
        nssf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getApplicationContext(), "sdd", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), NSSFReportActivity.class));
            }
        });

        nhif = (ImageView) findViewById(R.id.uap);
        nhif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getApplicationContext(), "sdd", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), NHIFReportActivity.class));
            }
        });


        uap = (ImageView) findViewById(R.id.nhif);
        uap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getApplicationContext(), "sdd", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), UAPReportActivity.class));
            }
        });


        akiliapp = (ImageView) findViewById(R.id.akiliapp);
        akiliapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getApplicationContext(), "sdd", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), MapsActivity.class));
            }
        });



        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
