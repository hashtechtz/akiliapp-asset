package com.akiliapp.hashtech.taitaremote.Vehicles;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.akiliapp.hashtech.taitaremote.R;
//import com.akiliapp.hashtech.taitarimoti.dummy.DummyContent;
import com.akiliapp.hashtech.taitaremote.Result;
import com.akiliapp.hashtech.taitaremote.Result_Node;
import com.akiliapp.hashtech.taitaremote.SplashScreen_Activity;
import com.akiliapp.hashtech.taitaremote.Vehicle_details_API;
import com.akiliapp.hashtech.taitaremote.Vehicledetails_POJO;
import com.akiliapp.hashtech.taitaremote.dummy.DummyContent;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class VehicleFragment extends Fragment {

    String reg_no;
    TextView item_number;
    //private String LOGIN_URL = "http://akiliapp.automechsaccos.co.tz/retrieve_vehicles.php";
    //private String LOGIN_URL_node = "http://akiliapp.com:3001/geocode_location";
    //private String LOGIN_URL_node = "http://akiliapp.com:3000/geocoder";
    private String LOGIN_URL_node = "http://148.251.138.82:3000/my_geocode_location_new";
//    private String LOGIN_URL_single = "http://148.251.138.82:3000/my_geocode_location_new_single";
//    private String LOGIN_URL_node = "http://akiliapp.com:3000/geocode_location_new";
//    private String LOGIN_URL_node = "http://akiliapp.com:3000/geocode_location_new_joan";
  //  private String ALL_VEHICLES = "http://akiliapp.com:3001/all_vehicles_list";

    Timer timer = new Timer();


    // TODO: Customize parameters
    private int mColumnCount = 1;

    private OnListFragmentInteractionListener mListener;

    Button button2;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public VehicleFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }


    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehicle_list, container, false);
        //    reg_no = getArguments().getString("reg_no");
//        String plate_number = getArguments().getString("plate_number");
        // item_number = (TextView) getView().findViewById(R.id.item_number);

        //item_number.setText(plate_number);

        //reg_no=getArguments().getString("reg_no");

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }



//            recyclerView.setAdapter(new VehicleRecyclerViewAdapter(context, DummyContent.ITEMS, mListener));
//            new java.util.Timer().schedule(
//                    new java.util.TimerTask() {
//                        @Override
//                        public void run() {
//            final long timeInterval = 5000;
//            Runnable runnable = new Runnable() {
//                public void run() {
//                    while (true) {
                           // cofirmationCodeSend();
//                        try {
//                            Thread.sleep(timeInterval);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            };
//            Thread thread = new Thread(runnable);
//            thread.start();
//                        }
//                    },
//                    5000
//            );
//

            TimerTask t = new TimerTask() {
                @Override
                public void run() {
                   // System.out.println("Hello World");
                    cofirmationCodeSend();
                }
//
            };
            timer.scheduleAtFixedRate(t,0,5000);

        }
        return view;
    }



    private void cofirmationCodeSend() {

//        new java.util.Timer().schedule(
//                new java.util.TimerTask() {
//                    @Override
//                    public void run() {


                    // ------- code for task to run
                   // System.out.println("Hello !!");
                    // ------- ends here


                        StringRequest request = new StringRequest(Request.Method.POST, LOGIN_URL_node,
                                new Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {

                                        try {

                                            JSONObject jsonObject = new JSONObject(response);
                                            JSONArray jsonArray = jsonObject.getJSONArray("results");

                                            List<Result_Node> resultList = new ArrayList<>();

                                            for(int i = 0; i < jsonArray.length(); i++) {

                                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);


                                                String device_time = jsonObject1.getString("device_time");
                                                String status_lock = jsonObject1.getString("status_lock");
                                                String plate_number = jsonObject1.getString("plate_number");
                                                String model = jsonObject1.getString("device_model");
                                                String name = jsonObject1.getString("name");
                                                String speed = jsonObject1.getString("speed");
                                                String uniqueId = jsonObject1.getString("uniqueId");
                                                String device_id = jsonObject1.getString("device_id");

                                                //for geocode_location_new
                                                String lastValidLongitude = jsonObject1.getString("lastValidLongitude");
                                                String lastValidLatitude = jsonObject1.getString("lastValidLatitude");
                                                String vin = jsonObject1.getString("vin");
//                                                String model = jsonObject1.getString("device_model");
//                                                String name = jsonObject1.getString("name");
//                                                String speed = jsonObject1.getString("speed");



//                                                String site = null;
//
//                                                if(longitude.equals("39.274826666667") && location.equals("-6.7682361111111")){
//                                                    site = "Namanga";
//                                                }



                                                //   Toast.makeText(getContext(),  ""+response, Toast.LENGTH_SHORT).show();

                                                Result_Node result = new Result_Node();
                                                result.setModel(model);
                                                result.setPlateNumber(plate_number);
                                                result.setLocation(status_lock);
                                                result.setSpeed(speed);
                                                result.setUniqueId(uniqueId);
                                                result.setDeviceTime(device_time);
                                                result.setName(name);
                                                result.setStatus_lock(status_lock);
                                                result.setDevice_id(device_id);
                                                result.setLastValidLatitude(lastValidLatitude);
                                                result.setLastValidLongitude(lastValidLongitude);
//                                                Result_Node result = new Result_Node();
                                                result.setVin(vin);
                                                //result.setVin(vin);
//                                                result.setLocation(location);
//                                                result.setSpeed(speed);
//                                                //result.setLong(site);
//                                                result.setLong(longitude);
//                                                result.setName(name);


                                                resultList.add(result);

                                                //Toast.makeText(getContext(),  result.setSpeed(speed), Toast.LENGTH_SHORT).show();


                                            }
                                            recyclerView.setAdapter(new VehicleRecyclerViewAdapter(getContext(), resultList, mListener));

                                        } catch (JSONException e) {
                                            // Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
                                          //  Toast.makeText(getContext(), ""+e, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }, new com.android.volley.Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                               // Toast.makeText(getContext(), "bb " + error, Toast.LENGTH_SHORT).show();
                                error.printStackTrace();


                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                  params.put("userphone", SplashScreen_Activity.sessionId);
                                //params.put("pass", pass);
                                return params;
                            }
                        };
                        Volley.newRequestQueue(getActivity()).add(request);

//                    }
//                }, 0, 10000);



    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            //throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        timer.cancel();
        timer.purge();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyContent.DummyItem item);
    }
}
