package com.akiliapp.hashtech.taitaremote;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button toggleButton;
    String URL_SWITCH = "https://akiliapp.com/api/send_gprs_command?user_api_hash=$2y$10$JqOIAN1H5UsK5/2VHXx3te80M4KvXWoVe/qeu0bD.KEGKban5ZtuW&lang=en&device_id=95&type=engineStop";

    TextView viewMap;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toggleButton = (Button) findViewById(R.id.toggleButton);
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cofirmationCodeSend();
               // startActivity(new Intent(getApplicationContext(), SwitchOn.class));


            }
        });

        viewMap = (TextView) findViewById(R.id.viewMap);
        viewMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), OpenLayer_Activity.class));
            }
        });
    }

    public void cofirmationCodeSend(){
        //  Toast.makeText(Menu_Option_Activity.this, userid, Toast.LENGTH_SHORT).show();
        StringRequest request = new StringRequest(Request.Method.POST, URL_SWITCH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), "Kifaa kimezima kikamilifu", Toast.LENGTH_SHORT).show();
                       // startActivity(new Intent(getApplicationContext(), MapsActivity.class));
                        imageView = (ImageView) findViewById(R.id.imageView);
                        imageView.setImageResource(R.mipmap.bodacontrolon);
                        startActivity(new Intent(getApplicationContext(), OpenLayer_Activity.class));

                        //   System.out.println(response);

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Kuna tatizo kifaa hakijapokea amri", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), OpenLayer_Activity.class));
                //error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
               // params.put("userid", "215");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }


}