package com.akiliapp.hashtech.taitaremote;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hbb20.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText phoneText;
    CountryCodePicker ccp;
    public static String userphone = "";
    private String LOGIN_URL = "http://148.251.138.82:3000/login_first_version";
//    private String LOGIN_URL = "http://akiliapp.automechsaccos.co.tz/login_verify_api.php";
    private TextWatcher text = null;
    List<Product> productList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        phoneText = new EditText(this);
        int maxLength = 3;
        phoneText.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});

        phoneText = (EditText) findViewById(R.id.phoneText);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);


        text = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                final int maxLength = 9;

                userphone = ccp.getSelectedCountryCode() + phoneText.getText().toString();

                if(phoneText.getText().toString().length() == maxLength) {
                    phoneText.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(maxLength) });


                    //login verification here
                  //  cofirmationCodeSend();
                    login_first_version();
                    //Toast.makeText(getApplicationContext(), userphone, Toast.LENGTH_SHORT).show();



                }




            }

            @Override
            public void afterTextChanged(Editable s) {
               // Button01.setEnabled(true);
              //  Button01.setBackgroundColor(Color.GREEN);
            }
        };

        phoneText.addTextChangedListener(text);



    }

    private void login_first_version(){
        StringRequest request = new StringRequest(Request.Method.POST, LOGIN_URL,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

              //  Toast.makeText(getApplicationContext(), ""+response, Toast.LENGTH_SHORT).show();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("real_user");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String real_user = jsonObject1.getString("real_user");
                    if(real_user.equals("0")){
                        Toast.makeText(getApplicationContext(), "Namba haitambuliki", Toast.LENGTH_SHORT).show();
                    }else if(!real_user.equals("0")){
                      //  Toast.makeText(getApplicationContext(), "Idadi: " + real_user, Toast.LENGTH_SHORT).show();
                        verifiedUser();

                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),""+e, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), "Samahani kuna tatizo la kimtandao", Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), ""+error, Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("userphone",  userphone);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }

    private void cofirmationCodeSend() {

        StringRequest request = new StringRequest(Request.Method.POST, LOGIN_URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                            String user_id = jsonObject1.getString("user_id");
                            String pass = jsonObject1.getString("password");
                            String fname = jsonObject1.getString("fname");
                            String mname = jsonObject1.getString("mname");
                            String lname = jsonObject1.getString("lname");
                            String driver_license = jsonObject1.getString("driver_license");
                            String phone = jsonObject1.getString("phone");
                           /* SharedPreferences shared = getSharedPreferences("Mac", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = shared.edit();
                            editor.putString("user_id",user_id);
                            editor.putString("pass",pass);
                            editor.putString("name",name);
                            editor.putString("driver_license",driver_license);
                            editor.putString("phone",phone);
                            editor.commit();*/

                           String fullname = fname + " " + lname;
                          // Toast.makeText(getApplicationContext(), "this is error" + user_id, Toast.LENGTH_SHORT).show();
                          Intent intent = new Intent(getApplicationContext(), VerifyActivity.class);
                            intent.putExtra("reg_no",user_id);
                            intent.putExtra("fullname",fullname);
                            intent.putExtra("password",pass);
                            startActivity(intent);



                            //     startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                        } catch (JSONException e) {
                          //  Toast.makeText(getApplicationContext(), "Phone number does not exist", Toast.LENGTH_SHORT).show();
                          //  Toast.makeText(getApplicationContext(), ""+e, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  Toast.makeText(getApplicationContext(), "this is error" + error, Toast.LENGTH_SHORT).show();
                //error.printStackTrace();


            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("phone", phoneText.getText().toString());
                //params.put("pass", pass);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }

    //select * vehicles owned by this person
   // String user_session;
    private void verifiedUser(){
       // user_session = "macco";
        SharedPreferences shared = getSharedPreferences("maco", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString("reg_no",userphone);
        editor.commit();
        //   Toast.makeText(getApplicationContext(), "yyy" + regno, Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(getApplicationContext(), Password_Confirmation.class);
        Intent intent = new Intent(getApplicationContext(), SplashScreen_Activity.class);
        intent.putExtra("reg_no",userphone);
        startActivity(intent);
        finish();
    }
}
