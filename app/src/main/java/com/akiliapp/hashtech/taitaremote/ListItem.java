package com.akiliapp.hashtech.taitaremote;

public class ListItem {

    private String plate_number;
    private String model;
    private String type_vehicle;

    public ListItem(String plate_number, String model, String type_vehicle){
        this.plate_number = plate_number;
        this.model = model;
        this.type_vehicle = type_vehicle;
    }
    public String getPlate_number(){return plate_number;}

    public String getModel() {return model;}

    public String getType_vehicle() {return type_vehicle;}
}
