package com.akiliapp.hashtech.taitaremote;

import android.content.Context;
import android.util.AttributeSet;

import androidx.core.content.ContextCompat;

import com.akiliapp.hashtech.taitaremote.R;
import com.akiliapp.hashtech.taitaremote.Reports_NSSF.data.Customer;

import de.codecrafters.tableview.SortableTableView;
import de.codecrafters.tableview.model.TableColumnWeightModel;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import de.codecrafters.tableview.toolkit.SortStateViewProviders;
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders;




/**
 * An extension of the {@link SortableTableView} that handles {@link Customer}s.
 *
 * @author ISchwarz
 */
public class New_ServiceSortableView extends SortableTableView<Customer> {

    public New_ServiceSortableView(final Context context) {
        this(context, null);
    }

    public New_ServiceSortableView(final Context context, final AttributeSet attributes) {
        this(context, attributes, android.R.attr.listViewStyle);
    }

    public New_ServiceSortableView(final Context context, final AttributeSet attributes, final int styleAttributes) {
        super(context, attributes, styleAttributes);

        final SimpleTableHeaderAdapter simpleTableHeaderAdapter = new SimpleTableHeaderAdapter(context, R.string.service, R.string.date_service, R.string.amount_paid, R.string.maelezo);
        simpleTableHeaderAdapter.setTextColor(ContextCompat.getColor(context, R.color.table_header_text_nssf));
        setHeaderAdapter(simpleTableHeaderAdapter);

        final int rowColorEven = ContextCompat.getColor(context, R.color.table_data_row_even);
        final int rowColorOdd = ContextCompat.getColor(context, R.color.table_data_row_odd);
        setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(rowColorEven, rowColorOdd));
        setHeaderSortStateViewProvider(SortStateViewProviders.brightArrows());

        final TableColumnWeightModel tableColumnWeightModel = new TableColumnWeightModel(4);
        tableColumnWeightModel.setColumnWeight(0, 4);
        tableColumnWeightModel.setColumnWeight(1, 4);
        tableColumnWeightModel.setColumnWeight(2, 4);
        tableColumnWeightModel.setColumnWeight(3, 4);
        //       tableColumnWeightModel.setColumnWeight(3, 2);
        setColumnModel(tableColumnWeightModel);

//        setColumnComparator(0, CustomerComparators.getCustomerProducerComparator());
//        setColumnComparator(1, CustomerComparators.getCustomerNameComparator());
//        setColumnComparator(2, CustomerComparators.getCustomerPowerComparator());
//        setColumnComparator(3, CustomerComparators.getCustomerPriceComparator());
    }

}

