package com.akiliapp.hashtech.taitaremote.Vehicles;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.akiliapp.hashtech.taitaremote.MapsActivity;
import com.akiliapp.hashtech.taitaremote.R;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class SwitchOffActivity extends AppCompatActivity {
    Button toggleButton;
    String URL_SWITCH = "https://akiliapp.com/api/send_gprs_command?user_api_hash=$2y$10$JqOIAN1H5UsK5/2VHXx3te80M4KvXWoVe/qeu0bD.KEGKban5ZtuW&lang=en&device_id=95&type=engineStop";

    String speed, model, plate_number;
    TextView model_viewer, plate_number_view, speed_viewer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_off);

        speed = getIntent().getStringExtra("speed");
        model = getIntent().getStringExtra("model");
        plate_number = getIntent().getStringExtra("plate_number");

        model_viewer = (TextView) findViewById(R.id.model_viewer);
        plate_number_view = (TextView) findViewById(R.id.plate_number_view);
        speed_viewer = (TextView) findViewById(R.id.speed_viewer);

        model_viewer.setText(model);
        plate_number_view.setText(plate_number);
        speed_viewer.setText(speed);

        toggleButton = (Button) findViewById(R.id.toggleButton);
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  cofirmationCodeSend();
               // startActivity(new Intent(getApplicationContext(), SwitchOn.class));
            }
        });
    }

    public void cofirmationCodeSend(){
        //  Toast.makeText(Menu_Option_Activity.this, userid, Toast.LENGTH_SHORT).show();
        StringRequest request = new StringRequest(Request.Method.POST, URL_SWITCH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), "Successfully switched OFF", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), MapsActivity.class));

                        //   System.out.println(response);

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "this is error " + error, Toast.LENGTH_SHORT).show();
                //error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
               // params.put("userid", "215");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }


}