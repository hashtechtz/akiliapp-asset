package com.akiliapp.hashtech.taitaremote;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

public class Password_Confirmation extends AppCompatActivity {

    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password__confirmation);
        logo = (ImageView) findViewById(R.id.logo);
        logo.setImageResource(R.drawable.ic_akiliapp);
    }
}
