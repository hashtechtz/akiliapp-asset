package com.akiliapp.hashtech.taitaremote.Reports_NSSF.data;


/**
 * Data object representing a customer.
 *
 * @author ISchwarz
 */
public class Customer implements Chargable {

    private final CustomerProducer producer;
    private final int ps;
//    private final double price;
    private String name;

    public Customer(final CustomerProducer producer, final String name, final int ps) {
        this.producer = producer;
        this.name = name;
        this.ps = ps;
//        this.price = price;
    }

    public CustomerProducer getProducer() {
        return producer;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getPs() {
        return ps;
    }

    public int getKw() {
        return (int) (ps / 1.36);
    }

//    public double getPrice() {
//        return price;
//    }

//    @Override
//    public String toString() {
//        return producer.getName() + " " + name;
//    }
}
