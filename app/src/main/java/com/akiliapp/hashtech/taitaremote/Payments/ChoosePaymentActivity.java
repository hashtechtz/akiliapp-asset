package com.akiliapp.hashtech.taitaremote.Payments;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.akiliapp.hashtech.taitaremote.Payments.PaymentInstallationActivity;
import com.akiliapp.hashtech.taitaremote.Payments.PaymentMonthlyActivity;
import com.akiliapp.hashtech.taitaremote.Payments.PaymentOneWeekActivity;
import com.akiliapp.hashtech.taitaremote.Payments.PaymentTwoWeeksActivity;
import com.akiliapp.hashtech.taitaremote.R;

public class ChoosePaymentActivity extends AppCompatActivity {

    ImageView oneweek, twoweeks, monthly, installation;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choosepayment_original);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
//      ab.setIcon(R.drawable.ic_boda_icon);
        ab.setTitle("Chagua Kifurushi");



        oneweek = (ImageView) findViewById(R.id.oneweek);
        oneweek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PaymentOneWeekActivity.class));
            }
        });

        twoweeks = (ImageView) findViewById(R.id.twoweeks);
        twoweeks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PaymentTwoWeeksActivity.class));
            }
        });

        monthly = (ImageView) findViewById(R.id.monthly);
        monthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PaymentMonthlyActivity.class));
            }
        });

        installation = (ImageView) findViewById(R.id.installation);
        installation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PaymentInstallationActivity.class));
            }
        });


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

}
