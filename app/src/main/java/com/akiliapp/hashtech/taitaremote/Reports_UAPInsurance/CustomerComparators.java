package com.akiliapp.hashtech.taitaremote.Reports_UAPInsurance;

import com.akiliapp.hashtech.taitaremote.Reports_UAPInsurance.data.Customer;

import java.util.Comparator;

//import de.codecrafters.tableviewexample.data.Customer;


/**
 * A collection of {@link Comparator}s for {@link Customer} objects.
 *
 * @author ISchwarz
 */
public final class CustomerComparators {

    private CustomerComparators() {
        //no instance
    }

    public static Comparator<Customer> getCustomerProducerComparator() {
        return new CustomerProducerComparator();
    }

    public static Comparator<Customer> getCustomerPowerComparator() {
        return new CustomerPowerComparator();
    }

    public static Comparator<Customer> getCustomerNameComparator() {
        return new CustomerNameComparator();
    }

//    public static Comparator<Customer> getCustomerPriceComparator() {
//        return new CustomerPriceComparator();
//    }


    private static class CustomerProducerComparator implements Comparator<Customer> {

        @Override
        public int compare(final Customer customer1, final Customer customer2) {
            return customer1.getProducer().getName().compareTo(customer2.getProducer().getName());
        }
    }

    private static class CustomerPowerComparator implements Comparator<Customer> {

        @Override
        public int compare(final Customer customer1, final Customer customer2) {
            return customer1.getPs() - customer2.getPs();
        }
    }

    private static class CustomerNameComparator implements Comparator<Customer> {

        @Override
        public int compare(final Customer customer1, final Customer customer2) {
            return customer1.getName().compareTo(customer2.getName());
        }
    }

//    private static class CustomerPriceComparator implements Comparator<Customer> {
//
//        @Override
//        public int compare(final Customer customer1, final Customer customer2) {
//            if (customer1.getPrice() < customer2.getPrice()) return -1;
//            if (customer1.getPrice() > customer2.getPrice()) return 1;
//            return 0;
//        }
//    }

}
