package com.akiliapp.hashtech.taitaremote;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.akiliapp.hashtech.taitaremote.Reports_NSSF.CustomerTableDataAdapter;
import com.akiliapp.hashtech.taitaremote.Reports_NSSF.NSSFReportActivity;
import com.akiliapp.hashtech.taitaremote.Reports_NSSF.data.Customer;
import com.akiliapp.hashtech.taitaremote.Reports_NSSF.data.DataFactory;

import java.util.List;
import java.util.Random;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.SwipeToRefreshListener;
import de.codecrafters.tableview.listeners.TableDataClickListener;
import de.codecrafters.tableview.listeners.TableDataLongClickListener;

public class New_ServiceActivity extends AppCompatActivity {

    Button help;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new__service);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
      //  help = (Button) findViewById(R.id.help);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
//      ab.setIcon(R.drawable.ic_boda_icon);
        ab.setTitle("Jaza taarifa za matengenezo hapa");

    }

}

