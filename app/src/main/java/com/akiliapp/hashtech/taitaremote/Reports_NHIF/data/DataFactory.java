package com.akiliapp.hashtech.taitaremote.Reports_NHIF.data;

import java.util.ArrayList;
import java.util.List;

/**
 * A factory that provides data for demonstration porpuses.
 *
 * @author ISchwarz
 */
public final class DataFactory {

    /**
     * Creates a list of customers.
     *
     * @return The created list of customers.
     */
    public static List<Customer> createCustomerList() {
        final CustomerProducer audi = new CustomerProducer( "1/2/2019");
        final Customer audiA1 = new Customer(audi, "Wiki 1", 150);
        final Customer audiA3 = new Customer(audi, "Wiki 2", 120);
        final Customer audiA4 = new Customer(audi, "Mwezi", 210);
        final Customer audiA5 = new Customer(audi, "Wiki 2", 333);
        final Customer audiA6 = new Customer(audi, "Mwezi", 250);
        final Customer audiA7 = new Customer(audi, "Wiki 2", 420);
        final Customer audiA8 = new Customer(audi, "Wiki 1", 320);

        final CustomerProducer bmw = new CustomerProducer( "3/4/2020");
        final Customer bmw1 = new Customer(bmw, "Mwezi", 170);
        final Customer bmw3 = new Customer(bmw, "Wiki 2", 230);
        final Customer bmwX3 = new Customer(bmw, "Wiki 1", 230);
        final Customer bmw4 = new Customer(bmw, "Mwezi", 250);
        final Customer bmwM4 = new Customer(bmw, "Wiki 2", 350);
        final Customer bmw5 = new Customer(bmw, "Wiki 1", 230);

        final CustomerProducer porsche = new CustomerProducer( "7/8/2030");
        final Customer porsche911 = new Customer(porsche, "Mwezi", 280);
        final Customer porscheCayman = new Customer(porsche, "Mwezi", 330);
        final Customer porscheCaymanGT4 = new Customer(porsche, "Mwezi", 385);

        final List<Customer> customers = new ArrayList<>();
        customers.add(audiA3);
        customers.add(audiA1);
        customers.add(porscheCayman);
        customers.add(audiA7);
        customers.add(audiA8);
        customers.add(audiA4);
        customers.add(bmwX3);
        customers.add(porsche911);
        customers.add(bmw1);
        customers.add(audiA6);
        customers.add(audiA5);
        customers.add(bmwM4);
        customers.add(bmw5);
        customers.add(porscheCaymanGT4);
        customers.add(bmw3);
        customers.add(bmw4);

        return customers;
    }

}
