package com.akiliapp.hashtech.taitaremote.Reports_UAPInsurance;

import android.content.Context;
import android.util.AttributeSet;

import com.akiliapp.hashtech.taitaremote.R;
import com.akiliapp.hashtech.taitaremote.Reports_UAPInsurance.data.Customer;

import androidx.core.content.ContextCompat;
import de.codecrafters.tableview.SortableTableView;
import de.codecrafters.tableview.model.TableColumnWeightModel;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import de.codecrafters.tableview.toolkit.SortStateViewProviders;
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders;


/**
 * An extension of the {@link SortableTableView} that handles {@link Customer}s.
 *
 * @author ISchwarz
 */
public class UAPSortableTableView extends SortableTableView<Customer> {

    public UAPSortableTableView(final Context context) {
        this(context, null);
    }

    public UAPSortableTableView(final Context context, final AttributeSet attributes) {
        this(context, attributes, android.R.attr.listViewStyle);
    }

    public UAPSortableTableView(final Context context, final AttributeSet attributes, final int styleAttributes) {
        super(context, attributes, styleAttributes);

        final SimpleTableHeaderAdapter simpleTableHeaderAdapter = new SimpleTableHeaderAdapter(context, R.string.date, R.string.packages, R.string.amount);
        simpleTableHeaderAdapter.setTextColor(ContextCompat.getColor(context, R.color.table_header_text));
        setHeaderAdapter(simpleTableHeaderAdapter);

        final int rowColorEven = ContextCompat.getColor(context, R.color.table_data_row_even);
        final int rowColorOdd = ContextCompat.getColor(context, R.color.table_data_row_odd);
        setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(rowColorEven, rowColorOdd));
        setHeaderSortStateViewProvider(SortStateViewProviders.brightArrows());

        final TableColumnWeightModel tableColumnWeightModel = new TableColumnWeightModel(3);
        tableColumnWeightModel.setColumnWeight(0, 3);
        tableColumnWeightModel.setColumnWeight(1, 3);
        tableColumnWeightModel.setColumnWeight(2, 3);
 //       tableColumnWeightModel.setColumnWeight(3, 2);
        setColumnModel(tableColumnWeightModel);

//        setColumnComparator(0, CustomerComparators.getCustomerProducerComparator());
//        setColumnComparator(1, CustomerComparators.getCustomerNameComparator());
//        setColumnComparator(2, CustomerComparators.getCustomerPowerComparator());
//        setColumnComparator(3, CustomerComparators.getCustomerPriceComparator());
    }

}
