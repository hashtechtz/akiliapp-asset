package com.akiliapp.hashtech.taitaremote.Vehicles;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akiliapp.hashtech.taitaremote.R;
import com.akiliapp.hashtech.taitaremote.Result_Node;
import com.akiliapp.hashtech.taitaremote.SplashScreen_Activity;
import com.akiliapp.hashtech.taitaremote.dummy.DummyContent;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.akiliapp.hashtech.taitarimoti.dummy.DummyContent;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class FamilyFragment extends Fragment {

    String reg_no;
    TextView item_number;
    //private String LOGIN_URL = "http://akiliapp.automechsaccos.co.tz/retrieve_vehicles.php";
    //private String LOGIN_URL_node = "http://akiliapp.com:3001/geocode_location";
    //private String LOGIN_URL_node = "http://akiliapp.com:3000/geocoder";
    private String LOGIN_URL_node = "http://148.251.138.82:3000/watch_details_api";
//    private String LOGIN_URL_node = "http://148.251.138.82:3000/my_geocode_location_new";
//    private String LOGIN_URL_single = "http://148.251.138.82:3000/my_geocode_location_new_single";
//    private String LOGIN_URL_node = "http://akiliapp.com:3000/geocode_location_new";
//    private String LOGIN_URL_node = "http://akiliapp.com:3000/geocode_location_new_joan";
  //  private String ALL_VEHICLES = "http://akiliapp.com:3001/all_vehicles_list";


    // TODO: Customize parameters
    private int mColumnCount = 1;

    private OnListFragmentInteractionListener mListener;

    Button button2;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FamilyFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }


    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_family, container, false);
        //    reg_no = getArguments().getString("reg_no");
//        String plate_number = getArguments().getString("plate_number");
        // item_number = (TextView) getView().findViewById(R.id.item_number);

        //item_number.setText(plate_number);

        //reg_no=getArguments().getString("reg_no");

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }



//            recyclerView.setAdapter(new VehicleRecyclerViewAdapter(context, DummyContent.ITEMS, mListener));
//            new java.util.Timer().schedule(
//                    new java.util.TimerTask() {
//                        @Override
//                        public void run() {
//            final long timeInterval = 5000;
//            Runnable runnable = new Runnable() {
//                public void run() {
//                    while (true) {
                           // cofirmationCodeSend();
//                        try {
//                            Thread.sleep(timeInterval);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            };
//            Thread thread = new Thread(runnable);
//            thread.start();
//                        }
//                    },
//                    5000
//            );
//
//            Timer t = new Timer();
//            t.schedule(new TimerTask() {
//                @Override
//                public void run() {
                   // System.out.println("Hello World");
                   // cofirmationCodeSend();
//                }
//            }, 0, 5000);

        }
        return view;
    }



    private void cofirmationCodeSend() {

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {


                    // ------- code for task to run
                   // System.out.println("Hello !!");
                    // ------- ends here


                        StringRequest request = new StringRequest(Request.Method.POST, LOGIN_URL_node,
                                new Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {

                                        try {

                                            JSONObject jsonObject = new JSONObject(response);
                                            JSONArray jsonArray = jsonObject.getJSONArray("result");

                                            List<Result_Node> resultList = new ArrayList<>();

                                            for(int i = 0; i < jsonArray.length(); i++) {

                                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);


                                                String location_single = jsonObject1.getString("location_single");
                                                String sim_number = jsonObject1.getString("sim_number");
                                                String child_name = jsonObject1.getString("child_name");
                                                String speed = jsonObject1.getString("speed");



                                                Result_Node result = new Result_Node();
                                                result.setLocation_single(location_single);
                                                result.setSim_number(sim_number);
                                                result.setChild_name(child_name);
                                                result.setSpeed(speed);



                                                resultList.add(result);

                                                //Toast.makeText(getContext(),  result.setSpeed(speed), Toast.LENGTH_SHORT).show();


                                            }
                                            recyclerView.setAdapter(new FamilyRecyclerViewAdapter(getContext(), resultList, (VehicleFragment.OnListFragmentInteractionListener) mListener));

                                        } catch (JSONException e) {
                                            // Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
                                            Toast.makeText(getContext(), "mm"+e, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getContext(), "bb " + error, Toast.LENGTH_SHORT).show();
                                error.printStackTrace();


                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                  params.put("userphone", SplashScreen_Activity.sessionId);
                                //params.put("pass", pass);
                                return params;
                            }
                        };
                        Volley.newRequestQueue(getActivity()).add(request);

                    }
                }, 0, 10000);



    }


//    private void cofirmationCodeSend() {
//
//        new java.util.Timer().schedule(
//                    new java.util.TimerTask() {
//                        @Override
//                        public void run() {
//
//       // Toast.makeText(getContext(), "did", Toast.LENGTH_SHORT).show();
//
//
//
//
//
//        StringRequest request = new StringRequest(Request.Method.GET, LOGIN_URL_node,
//                new Response.Listener<String>() {
//
//                    @Override
//                    public void onResponse(String response) {
//
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            JSONArray jsonArray = jsonObject.getJSONArray("messages");
//
//                            List<Result_Node> resultList = new ArrayList<>();
//
//                            for(int i = 0; i < jsonArray.length(); i++) {
//
//                                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
//                                // String imei = jsonObject1.getString("imei");
//                                String location = jsonObject1.getString("location");
//                                String plate_number = jsonObject1.getString("plate_number");
//                                String model = jsonObject1.getString("model");
//                                String speed = jsonObject1.getString("speed");
//
//                               //   Toast.makeText(getContext(),  plate_number, Toast.LENGTH_SHORT).show();
//
//                                Result_Node result = new Result_Node();
//                                result.setModel(model);
//                                result.setPlateNumber(plate_number);
//                                result.setLocation(location);
//                                result.setSpeed(speed);
//
//
//                                resultList.add(result);
//
//
//                            }
//                            recyclerView.setAdapter(new VehicleRecyclerViewAdapter(getContext(), resultList, mListener));
//
//                        } catch (JSONException e) {
//                            // Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
//                              Toast.makeText(getContext(), ""+e, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }, new com.android.volley.Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                   Toast.makeText(getContext(), "bb " + error, Toast.LENGTH_SHORT).show();
//                error.printStackTrace();
//
//
//            }
//        }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//              //  params.put("regno", SplashScreen_Activity.sessionId);
//                //params.put("pass", pass);
//                return params;
//            }
//        };
//        Volley.newRequestQueue(getActivity()).add(request);
//
//                        }
//                    }, 0, 5000);
//    }



//    public void details_vehicles(){
////        // to_trip.setText("masaki");
////        int radioId = radioGroup.getCheckedRadioButtonId();
////        radioButton = (RadioButton) findViewById(radioId);
////        if(radioButton.getText().equals("Bodaboda")){
////            mabi = "Boda Boda";
////
////        }if(radioButton.getText().equals("Bajaj")){
////            mabi = "Bajaj";
////        }
////        if(radioButton.getText().equals("Kirikuu")){
////            mabi = "Kirikuu";
////
////        }if(radioButton.getText().equals("Fuso")){
////            mabi = "Fuso";
////        }
//        //connect to my api mac mwocha
//        OkHttpClient client = new OkHttpClient();
//        Gson gson = new GsonBuilder()
//                .setLenient()
//                .create();
////dedication to mama bhoke 09/01/2019 insert data to database
//        Retrofit retrofit = new Retrofit.Builder()
//                // .baseUrl("http://192.168.43.147/") //babi connection
//                // .baseUrl("http://192.168.43.183/")//mabi connection
//                .baseUrl(LOGIN_URL_node) //hashtech connection
//                .client(client)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build();
//        Vehicle_details_API service = retrofit.create(Vehicle_details_API.class);
//        Vehicledetails_POJO art = new Vehicledetails_POJO();
////        art.setPlate_number(from_trip.getText().toString());
////        art.setToTrip(to_trip.getText().toString());
////        art.setVehicle(mabi);
//
//        Call<Vehicledetails_POJO> call = service.insertData(art.getName(),art.getPlate_number(),art.getUniqueId(),art.getRegistration_number(),art.getLastValidLatitude(),art.getLastValidLatitude(),art.getRegistration_number());
//
//        call.enqueue(new Callback<Vehicledetails_POJO>() {
//            @Override
//            public void onResponse(Call<Vehicledetails_POJO> call, retrofit2.Response<Abiria_Request_trip> response) {
//
//                Toast.makeText(Abiria.this, "response"+response, Toast.LENGTH_LONG).show();
//
//                from_trip.setText("");
//                to_trip.setText("");
//
//
//            }
//
//            @Override
//            public void onFailure(Call<Abiria_Request_trip> call, Throwable t) {
//
//
//                //  Log.i("Hello",""+t);
//                Toast.makeText(Abiria.this, "Request sent...Please wait for driver near to accept ", Toast.LENGTH_LONG).show();
//                from_trip.setText("");
//                to_trip.setText("");
//            }
//        });
//    }
//


//    private void cofirmationCodeSend() {
//
//        StringRequest request = new StringRequest(Request.Method.POST, LOGIN_URL,
//                new Response.Listener<String>() {
//
//                    @Override
//                    public void onResponse(String response) {
//
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            JSONArray jsonArray = jsonObject.getJSONArray("result");
//
//                            List<Result> resultList = new ArrayList<>();
//
//                            for(int i = 0; i < jsonArray.length(); i++) {
//
//                                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
//                                // String imei = jsonObject1.getString("imei");
//                                String plate_number = jsonObject1.getString("plate_number");
//                                String type_vehicle = jsonObject1.getString("type_vehicle");
//                                String model = jsonObject1.getString("model");
//
//                              ///  Toast.makeText(getContext(),  plate_number, Toast.LENGTH_SHORT).show();
//
//                                Result result = new Result();
//                                result.setModel(model);
//                                result.setPlateNumber(plate_number);
//                                result.setTypeVehicle(type_vehicle);
//
//
//                                resultList.add(result);
//
//
//
//                                SharedPreferences shared = getContext().getSharedPreferences("Mac", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = shared.edit();
//                                //  editor.putString("imei", imei);
//                                editor.putString("plate_number", plate_number);
//                                editor.putString("type_vehicle", type_vehicle);
//                                editor.putString("model", model);
//                                //  editor.putString("device_id", device_id);
//                                editor.commit();
//
//                            }
//                            recyclerView.setAdapter(new VehicleRecyclerViewAdapter(getContext(), resultList, mListener));
//
//                        } catch (JSONException e) {
//                           // Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
//                            //  Toast.makeText(getContext()(), ""+e, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }, new com.android.volley.Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//             //   Toast.makeText(getContext(), "bb " + error, Toast.LENGTH_SHORT).show();
//                error.printStackTrace();
//
//
//            }
//        }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("regno", SplashScreen_Activity.sessionId);
//                //params.put("pass", pass);
//                return params;
//            }
//        };
//        Volley.newRequestQueue(getActivity()).add(request);
//    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            //throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyContent.DummyItem item);
    }
}
