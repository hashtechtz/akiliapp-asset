package com.akiliapp.hashtech.taitaremote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result_Map {


        @SerializedName("lastValidLatitude")
    @Expose
    private String lastValidLatitude;
    @SerializedName("lastValidLongitude")
    @Expose
    private String lastValidLongitude;


    public String getLastValidLatitude() {
        return lastValidLatitude;
    }

    public void setLastValidLatitude(String lastValidLatitude) {
        this.lastValidLatitude = lastValidLatitude;
    }

    public String getLastValidLongitude() {
        return lastValidLongitude;
    }

    public void setLastValidLongitude(String lastValidLongitude) {
        this.lastValidLongitude = lastValidLongitude;
    }




}
