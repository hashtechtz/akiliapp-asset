package com.akiliapp.hashtech.taitaremote;

public class Product {

    private String reg_no;
    private String fname;
    private String mname;
    private String lname;
    private String password;

    public Product(String reg_no, String fname, String mname, String lname, String password) {
        this.reg_no = reg_no;
        this.fname = fname;
        this.mname = mname;
        this.lname = lname;
        this.password = password;

    }

    public String getRegno() {
        return reg_no;
    }

    public String getTFname() {
        return fname;
    }

    public String getMname() {
        return lname;
    }

    public String getLname() {
        return lname;
    }

    public String getPassword() {
        return password;
    }
}
