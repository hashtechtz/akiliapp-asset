package com.akiliapp.hashtech.taitaremote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result_Node {




    @SerializedName("sim_number")
    @Expose
    private String sim_number;
    @SerializedName("child_name")
    @Expose
    private String child_name;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("vin")
    @Expose
    private String vin;
    @SerializedName("angle")
    @Expose
    private String angle;
    @SerializedName("device_id")
    @Expose
    private String device_id;
    @SerializedName("status_lock")
    @Expose
    private String status_lock;
    @SerializedName("location_single")
    @Expose
    private String location_single;
    @SerializedName("uniqueId")
    @Expose
    private String uniqueId;
    @SerializedName("lastValidLongitude")
    @Expose
    private String lastValidLongitude;
    @SerializedName("plate_number")
    @Expose
    private String plate_number;
    @SerializedName("device_model")
    @Expose
    private String model;
    @SerializedName("lastValidLatitude")
    @Expose
    private String lastValidLatitude;
    @SerializedName("speed")
    @Expose
    private String speed;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("device_time")
    @Expose
    private String device_time;

    public String getPlateNumber() {
        return plate_number;
    }

    public String getSim_number() {
        return sim_number;
    }

    public void setSim_number(String sim_number) {
        this.sim_number = sim_number;
    }

    public String getChild_name() {
        return child_name;
    }

    public void setChild_name(String child_name) {
        this.child_name = child_name;
    }

    public void setPlateNumber(String plateNumber) {
        this.plate_number = plateNumber;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation_single() {
        return location_single;
    }

    public void setLocation_single(String location_single) {
        this.location_single = location_single;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastValidLongitude() {
        return lastValidLongitude;
    }

    public void setLastValidLongitude(String lastValidLongitude) {
        this.lastValidLongitude = lastValidLongitude;
    }

    public String getLastValidLatitude() {
        return lastValidLatitude;
    }

    public void setLastValidLatitude(String lastValidLatitude) {
        this.lastValidLatitude = lastValidLatitude;
    }

    public String getDeviceTime() {
        return device_time;
    }

    public void setDeviceTime(String device_time) {
        this.device_time = device_time;
    }

    public String getStatus_lock() {
        return status_lock;
    }

    public void setStatus_lock(String status_lock) {
        this.status_lock = status_lock;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getAngle() {
        return angle;
    }

    public void setAngle(String angle) {
        this.angle = angle;
    }


}
