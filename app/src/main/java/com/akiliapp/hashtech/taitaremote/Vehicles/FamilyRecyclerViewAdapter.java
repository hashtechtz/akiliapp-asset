package com.akiliapp.hashtech.taitaremote.Vehicles;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.akiliapp.hashtech.taitaremote.OpenLayer_Activity;
import com.akiliapp.hashtech.taitaremote.R;
import com.akiliapp.hashtech.taitaremote.Result;
import com.akiliapp.hashtech.taitaremote.Result_Node;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Result} and makes a call to the
 * specified {@link VehicleFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class FamilyRecyclerViewAdapter extends RecyclerView.Adapter<FamilyRecyclerViewAdapter.ViewHolder> {


    public static final String KEY_NAME = "name";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_URL = "url";

//    private String LOGIN_URL_node = "http://148.251.138.82:3000/tester";
      private String LOGIN_URL_node = "http://148.251.138.82:3000/geocode_location_new";
      private String Locater = "http://148.251.138.82:3000/vehicle_details_name";
    String URL_SWITCH = "https://akiliapp.com/api/send_gprs_command?user_api_hash=$2y$10$JqOIAN1H5UsK5/2VHXx3te80M4KvXWoVe/qeu0bD.KEGKban5ZtuW&lang=en&device_id=95&type=engineStop";

    private List<VehiclesList> vehiclesLists;
    private Context mcontext;

    //  public VehiclesList

    private final List<Result_Node> mValues;
    private final VehicleFragment.OnListFragmentInteractionListener mListener;
    public Button button2, button3;
    public static String imei = "";

    TextView my_account;
    private AdapterView.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void SetOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = (AdapterView.OnItemClickListener) listener;
    }

    public FamilyRecyclerViewAdapter(Context contextl, List<Result_Node> items, VehicleFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        mcontext = contextl;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_family_data, parent, false);
        return new ViewHolder(view);





    }

    private void dialContactPhone(final String phoneNumber) {
        mcontext.startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        holder.textViewVehicleModel.setText(mValues.get(position).getChild_name());
        holder.textView8.setText(mValues.get(position).getLocation_single());

        holder.imageView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialContactPhone(mValues.get(position).getSim_number());
            }
        });

        if(mValues.get(position).getSpeed().equals("0")) {
            holder.textView16.setText("Amesimama");
            holder.status_vehicle.setImageResource(R.drawable.ic_stopped);
        }

        else if(mValues.get(position).getSpeed().equals("null")){
            holder.mContentView.setText("Stopped");
            holder.status_vehicle.setImageResource(R.drawable.ic_offline);

        }
        else {
            holder.mContentView.setText("Anatembea " + mValues.get(position).getSpeed() + "kph");
            holder.status_vehicle.setImageResource(R.drawable.ic_moving);
        }




//        holder.mItem = mValues.get(position);
//        holder.mIdView.setText(mValues.get(position).getPlateNumber());
//        holder.textViewVehicleModel.setText(mValues.get(position).getModel());
//        holder.my_account.setText(mValues.get(position).getName());
//       // holder.uniqueid.setText(mValues.get(position).getUniqueId());
//       // holder.uniqueid.setText("lat: "+  mValues.get(position).getLastValidLatitude() + " long: " + mValues.get(position).getLastValidLongitude());
//        holder.device_id.setText(mValues.get(position).getDevice_id());
//        if(mValues.get(position).getVin().equals("4") && mValues.get(position).getStatus_lock().equals("0")) {
//            holder.imageView4.setImageResource(R.drawable.ic_car_off);
//        }else if(mValues.get(position).getVin().equals("3") && mValues.get(position).getStatus_lock().equals("0")) {
//            holder.imageView4.setImageResource(R.drawable.ic_bajaj_off);
//        }else if(mValues.get(position).getVin().equals("2") && mValues.get(position).getStatus_lock().equals("0")) {
//            holder.imageView4.setImageResource(R.drawable.ic_boda_off);
//        } if(mValues.get(position).getVin().equals("4") && mValues.get(position).getStatus_lock().equals("1")) {
//            holder.imageView4.setImageResource(R.drawable.ic_car_on);
//        }else if(mValues.get(position).getVin().equals("3") && mValues.get(position).getStatus_lock().equals("1")) {
//            holder.imageView4.setImageResource(R.drawable.ic_bajaj_on);
//        }else if(mValues.get(position).getVin().equals("2") && mValues.get(position).getStatus_lock().equals("1")) {
//            holder.imageView4.setImageResource(R.drawable.ic_boda_boda);
//        }
//
//
//
//        if(mValues.get(position).getStatus_lock().equals("1")) {
//            holder.button3.setBackgroundResource(R.drawable.ic_zima);
//
//
//
//        }if(mValues.get(position).getStatus_lock().equals("0")) {
//            holder.button3.setBackgroundResource(R.drawable.ic_washa);
//        }
//        if(mValues.get(position).getSpeed().equals("0")){
//            holder.mContentView.setText(mValues.get(position).getSpeed()+"kph");
//            holder.status_vehicle.setImageResource(R.drawable.ic_stopped);
//
//        }
//        else if(mValues.get(position).getSpeed().equals("null")){
//            holder.mContentView.setText("Stopped");
//            holder.status_vehicle.setImageResource(R.drawable.ic_offline);
//
//        }
//        else {
//            holder.mContentView.setText("Inatembea " + mValues.get(position).getSpeed() + "kph");
//            holder.status_vehicle.setImageResource(R.drawable.ic_moving);
//        }
//
//
//
//
//       // holder.location_viewer.setText((mValues.get(position).getDevice_id()));
//       // holder.location_viewer.setText((mValues.get(position).getDeviceTime()));
//        //holder.button3.setImageResource(R.drawable.ic_moving);
//


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imei = mValues.get(position).getUniqueId();
              //  holder.uniqueid.setText(mValues.get(position).getLocation_single());
              //  Toast.makeText(mcontext.getApplicationContext(),  mValues.get(position).getLocation_single() + "lat: "+  mValues.get(position).getLastValidLatitude() + " long: " + mValues.get(position).getLastValidLongitude(), Toast.LENGTH_SHORT).show();

                new Timer().schedule(
                new TimerTask() {
                    @Override
                    public void run() {

              //  ((OpenLayer_Activity) mcontext).loadMapSingleDevice();
                ((OpenLayer_Activity) mcontext).SingleDeviceMethod();


                StringRequest request = new StringRequest(Request.Method.POST, Locater,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {

                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONArray jsonArray = jsonObject.getJSONArray("result");

                                    List<Result_Node> resultList = new ArrayList<>();

                                   // for(int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(0);

                                        //for geocoder
                                        String location_single = jsonObject1.getString("location_single");
                                        String speed = jsonObject1.getString("speed");

                                        Result_Node result = new Result_Node();
                                        result.setLocation_single(location_single);
                                        result.setSpeed(speed);


                                        resultList.add(result);

                                      // Toast.makeText(mcontext.getApplicationContext(),  "eee"+location_single, Toast.LENGTH_LONG).show();
                                    holder.uniqueid.setText(location_single);

                                    if(mValues.get(position).getSpeed().equals("0")){
                                        holder.mContentView.setText(mValues.get(position).getSpeed()+"kph");
                                        holder.status_vehicle.setImageResource(R.drawable.ic_stopped);

                                    }
                                    else if(mValues.get(position).getSpeed().equals("null")){
                                        holder.mContentView.setText("Stopped");
                                        holder.status_vehicle.setImageResource(R.drawable.ic_offline);

                                    }
                                    else {
                                        holder.mContentView.setText("Inatembea " + mValues.get(position).getSpeed() + "kph");
                                        holder.status_vehicle.setImageResource(R.drawable.ic_moving);
                                    }

                                   // }


                                } catch (JSONException e) {
                                   //  Toast.makeText(mcontext.getApplicationContext(), "nnn"+e, Toast.LENGTH_SHORT).show();
                                    //  Toast.makeText(getContext(), ""+e, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      //   Toast.makeText(mcontext.getApplicationContext(), "bb " + error, Toast.LENGTH_SHORT).show();
                        error.printStackTrace();


                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                          params.put("uniqueId", mValues.get(position).getUniqueId());
                        //params.put("pass", pass);
                        return params;
                    }
                };
                Volley.newRequestQueue(mcontext.getApplicationContext()).add(request);
              //  holder.location_viewer.setText(VehicleRecyclerViewAdapter.imei);


            }
        }, 0, 10000);

            }

        });
//        holder.button2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mValues.get(position).getStatus_lock().equals("1")){
//                    // Toast.makeText(mcontext, "Test", Toast.LENGTH_SHORT).show();
//                    //   mcontext.startActivity(new Intent(mcontext, SwitchOffActivity.class));
//                    Intent intent = new Intent(mcontext, SwitchOffActivity.class);
//                    intent.putExtra("speed", mValues.get(position).getSpeed());
//                    intent.putExtra("model", mValues.get(position).getSpeed());
//                    intent.putExtra("plate_number", mValues.get(position).getSpeed());
//                    mcontext.startActivity(intent);
//            }
//                if (mValues.get(position).getStatus_lock().equals("0")){
//                    // Toast.makeText(mcontext, "Test", Toast.LENGTH_SHORT).show();
//                    //   mcontext.startActivity(new Intent(mcontext, SwitchOffActivity.class));
//                    Intent intent = new Intent(mcontext, SwitchOnActivity.class);
//                    intent.putExtra("speed", mValues.get(position).getSpeed());
//                    intent.putExtra("model", mValues.get(position).getSpeed());
//                    intent.putExtra("plate_number", mValues.get(position).getSpeed());
//                    mcontext.startActivity(intent);
//                }
//               // holder.imageButton.setImageResource(R.drawable.ic_switch_off_pink);
//               // holder.button3.setBackground(R.drawable.ic_switch_off_pink);
////                holder.button2.setBackgroundResource(R.drawable.ic_washa);
////                holder.button3.setBackgroundDrawable(mcontext.getResources().getDrawable(R.drawable.ic_switch_off_pink));
//
//            }
//        });
//        holder.button3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(mValues.get(position).getStatus_lock().equals("1")) {
//                StringRequest request = new StringRequest(Request.Method.POST, "http://148.251.138.82/api/send_gprs_command?lang=en&user_api_hash=$2y$10$JqOIAN1H5UsK5/2VHXx3te80M4KvXWoVe/qeu0bD.KEGKban5ZtuW&device_id=" + mValues.get(position).getDevice_id() + "&type=engineStop", new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                                StringRequest request1 = new StringRequest(Request.Method.POST, "http://148.251.138.82:3000/switchoff_engine",
//                                        new Response.Listener<String>() {
//                                            @Override
//                                            public void onResponse(String response) {
//
//                                                Toast.makeText(mcontext.getApplicationContext(), mValues.get(position).getPlateNumber() + " imezima kikamilifu", Toast.LENGTH_LONG).show();
//
//                                            }
//                                        }, new Response.ErrorListener() {
//                                    @Override
//                                    public void onErrorResponse(VolleyError error) {
//                                        Toast.makeText(mcontext.getApplicationContext(), "this is error " + error, Toast.LENGTH_SHORT).show();
//                                    }
//                                }){
//                                    @Override
//                                    protected Map<String, String> getParams() {
//                                        Map<String, String> params = new HashMap<>();
//                                         params.put("device_id", mValues.get(position).getDevice_id());
//                                        return params;
//                                    }
//                                };
//                                Volley.newRequestQueue(mcontext.getApplicationContext()).add(request1);
//                      //  Toast.makeText(mcontext.getApplicationContext(), mValues.get(position).getPlateNumber() + " imezima kikamilifu", Toast.LENGTH_SHORT).show();
//
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                      //  Toast.makeText(mcontext.getApplicationContext(), "this is error " + error, Toast.LENGTH_SHORT).show();
//                    }
//                }) {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<>();
//                        // params.put("userid", "215");
//                        return params;
//                    }
//                };
//                Volley.newRequestQueue(mcontext.getApplicationContext()).add(request);
//                holder.button3.setBackgroundResource(R.drawable.ic_washa);
//                if(mValues.get(position).getVin().equals("4")) {
//                    holder.imageView4.setImageResource(R.drawable.ic_car_off);
//                }else if(mValues.get(position).getVin().equals("3")) {
//                    holder.imageView4.setImageResource(R.drawable.ic_bajaj_off);
//                }else if(mValues.get(position).getVin().equals("2")) {
//                    holder.imageView4.setImageResource(R.drawable.ic_boda_off);
//                }
//
//            }
//
//                if(mValues.get(position).getStatus_lock().equals("0")) {
//                StringRequest request = new StringRequest(Request.Method.POST, "http://148.251.138.82/api/send_gprs_command?lang=en&user_api_hash=$2y$10$JqOIAN1H5UsK5/2VHXx3te80M4KvXWoVe/qeu0bD.KEGKban5ZtuW&device_id=" + mValues.get(position).getDevice_id() + "&type=engineResume", new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                                StringRequest request1 = new StringRequest(Request.Method.POST, "http://148.251.138.82:3000/switchon_engine",
//                                        new Response.Listener<String>() {
//                                            @Override
//                                            public void onResponse(String response) {
//
//                                                Toast.makeText(mcontext.getApplicationContext(), mValues.get(position).getPlateNumber() + " imewashwa kikamilifu", Toast.LENGTH_SHORT).show();
//
//                                            }
//                                        }, new Response.ErrorListener() {
//                                    @Override
//                                    public void onErrorResponse(VolleyError error) {
//                                        Toast.makeText(mcontext.getApplicationContext(), "this is error " + error, Toast.LENGTH_SHORT).show();
//                                    }
//                                }){
//                                    @Override
//                                    protected Map<String, String> getParams() {
//                                        Map<String, String> params = new HashMap<>();
//                                         params.put("device_id", mValues.get(position).getDevice_id());
//                                        return params;
//                                    }
//                                };
//                                Volley.newRequestQueue(mcontext.getApplicationContext()).add(request1);
////                        Toast.makeText(mcontext.getApplicationContext(), mValues.get(position).getPlateNumber() + " imewashwa kikamilifu", Toast.LENGTH_SHORT).show();
//
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                      //  Toast.makeText(mcontext.getApplicationContext(), "this is error " + error, Toast.LENGTH_SHORT).show();
//                    }
//                }) {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<>();
//                        // params.put("userid", "215");
//                        return params;
//                    }
//                };
//                Volley.newRequestQueue(mcontext.getApplicationContext()).add(request);
//
//                holder.button3.setBackgroundResource(R.drawable.ic_zima);
//                    if(mValues.get(position).getVin().equals("4")) {
//                        holder.imageView4.setImageResource(R.drawable.ic_car_on);
//                    }else if(mValues.get(position).getVin().equals("3")) {
//                        holder.imageView4.setImageResource(R.drawable.ic_bajaj_on);
//                    }else if(mValues.get(position).getVin().equals("2")) {
//                        holder.imageView4.setImageResource(R.drawable.ic_bodaboda);
//                    }
//
//            }
//            }
//        });
//        holder.my_account.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               // mcontext.startActivity(new Intent(mcontext, MyAccountOrginalActivity.class));
//              //  Toast.makeText(mcontext.getApplicationContext(),"jj", Toast.LENGTH_SHORT).show();
//            }
//        });


    }



    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public Result_Node mItem;
        private Button button2, button3;
        private TextView my_account, textView8, textView16;
        private TextView location_viewer,uniqueid;
        private TextView textViewVehicleModel, device_id;
        private ImageView status_vehicle, imageView4, imageView8;
        private ImageButton imageButton;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
//            button2 = (Button) view.findViewById(R.id.button2);
            button3 = (Button) view.findViewById(R.id.button3);
            my_account = (TextView) view.findViewById(R.id.my_account);
            textViewVehicleModel = (TextView) view.findViewById(R.id.textViewVehicleModel);
            location_viewer = (TextView) view.findViewById((R.id.location_viewer));
            uniqueid = (TextView) view.findViewById((R.id.uniqueid));
            status_vehicle = (ImageView) view.findViewById(R.id.status_vehicle);
            device_id = (TextView) view.findViewById(R.id.device_id);
            imageView4 = (ImageView) view.findViewById(R.id.imageView4);
            imageView8 = (ImageView) view.findViewById(R.id.imageView8);
            textView8 = (TextView) view.findViewById(R.id.textView8);
            textView16 = (TextView) view.findViewById(R.id.textView16);


        }

        @Override
        public String toString() { return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

//        //switch off method
//        public void stopEngineCommand(){
//            //  Toast.makeText(Menu_Option_Activity.this, userid, Toast.LENGTH_SHORT).show();
//            StringRequest request = new StringRequest(Request.Method.POST, URL_SWITCH,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            Toast.makeText(mcontext.getApplicationContext(), "Kifaa kimezima kikamilifu", Toast.LENGTH_SHORT).show();
//
//
//                        }
//                    }, new com.android.volley.Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(mcontext.getApplicationContext(), "Kuna tatizo kifaa hakijapokea amri", Toast.LENGTH_SHORT).show();
//                    //startActivity(new Intent(getApplicationContext(), OpenLayer_Activity.class));
//                    //error.printStackTrace();
//                }
//            }){
//                @Override
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<>();
//                    // params.put("userid", "215");
//                    return params;
//                }
//            };
//            Volley.newRequestQueue(this).add(request);
//        }

}
