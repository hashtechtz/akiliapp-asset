package com.akiliapp.hashtech.taitaremote;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.akiliapp.hashtech.taitaremote.Vehicles.FamilyRecyclerViewAdapter;
import com.akiliapp.hashtech.taitaremote.Vehicles.VehicleFragment;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuActivity extends AppCompatActivity {

    ImageView propertyCategoryMenu, familyCategoryMenu;
    private String LOGIN_URL_node = "http://148.251.138.82:3000/watch_details_api";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        propertyCategoryMenu = (ImageView) findViewById(R.id.propertyCategoryMenu);
        propertyCategoryMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, OpenLayer_Activity.class);
                //intent.putExtra("EXTRA_SESSION_ID", val);
                startActivity(intent);
            }
        });

        familyCategoryMenu = (ImageView) findViewById(R.id.familyCategoryMenu);
        familyCategoryMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                StringRequest request = new StringRequest(Request.Method.POST, LOGIN_URL_node,
//                        new Response.Listener<String>() {
//
//                            @Override
//                            public void onResponse(String response) {



                                        Intent intent = new Intent(MenuActivity.this, Family_Activity.class);
                                        //intent.putExtra("EXTRA_SESSION_ID", val);
                                        startActivity(intent);

//
//                            }
//                        }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getApplicationContext(), "Sahamani hauna wanafamilia wa kuwaangalia", Toast.LENGTH_SHORT).show();
//                        error.printStackTrace();
//
//
//                    }
//                }){
//                    @Override
//                    protected Map<String, String> getParams() throws AuthFailureError {
//                        Map<String, String> params = new HashMap<>();
//                        params.put("userphone", SplashScreen_Activity.sessionId);
//                        //params.put("pass", pass);
//                        return params;
//                    }
//                };
//                Volley.newRequestQueue(getApplicationContext()).add(request);


            }
        });
    }
}
