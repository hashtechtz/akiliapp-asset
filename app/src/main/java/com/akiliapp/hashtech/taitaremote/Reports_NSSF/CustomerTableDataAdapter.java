package com.akiliapp.hashtech.taitaremote.Reports_NSSF;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.akiliapp.hashtech.taitaremote.R;
import com.akiliapp.hashtech.taitaremote.Reports_NSSF.data.Customer;

import java.text.NumberFormat;
import java.util.List;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.LongPressAwareTableDataAdapter;
//import java.util.List;
//
//import de.codecrafters.tableview.TableView;
//import de.codecrafters.tableview.toolkit.LongPressAwareTableDataAdapter;
//import de.codecrafters.tableviewexample.data.Customer;


public class CustomerTableDataAdapter extends LongPressAwareTableDataAdapter<Customer> {

    private static final int TEXT_SIZE = 14;
    private static final NumberFormat PRICE_FORMATTER = NumberFormat.getNumberInstance();


    public CustomerTableDataAdapter(final Context context, final List<Customer> data, final TableView<Customer> tableView) {
        super(context, data, tableView);
    }

    @Override
    public View getDefaultCellView(int rowIndex, int columnIndex, ViewGroup parentView) {
        final Customer customer = getRowData(rowIndex);
        View renderedView = null;

        switch (columnIndex) {
            case 0:
                renderedView = renderProducerLogo(customer, parentView);
                break;
            case 1:
                renderedView = renderCatName(customer);
                break;
            case 2:
                renderedView = renderPower(customer, parentView);
                break;
//            case 3:
//                renderedView = renderPrice(customer);
//                break;
        }

        return renderedView;
    }

    @Override
    public View getLongPressCellView(int rowIndex, int columnIndex, ViewGroup parentView) {
        final Customer customer = getRowData(rowIndex);
        View renderedView = null;

        switch (columnIndex) {
            case 1:
                renderedView = renderEditableCatName(customer);
                break;
            default:
                renderedView = getDefaultCellView(rowIndex, columnIndex, parentView);
        }

        return renderedView;
    }

    private View renderEditableCatName(final Customer customer) {
        final EditText editText = new EditText(getContext());
        editText.setText(customer.getName());
        editText.setPadding(20, 10, 20, 10);
        editText.setTextSize(TEXT_SIZE);
        editText.setSingleLine();
        editText.addTextChangedListener(new CustomerNameUpdater(customer));
        return editText;
    }

//    private View renderPrice(final Customer customer) {
//        final String priceString = PRICE_FORMATTER.format(customer.getPrice()) + " €";
//
//        final TextView textView = new TextView(getContext());
//        textView.setText(priceString);
//        textView.setPadding(20, 10, 20, 10);
//        textView.setTextSize(TEXT_SIZE);
//
//        if (customer.getPrice() < 50000) {
//            textView.setTextColor(ContextCompat.getColor(getContext(), R.color.table_price_low));
//        } else if (customer.getPrice() > 100000) {
//            textView.setTextColor(ContextCompat.getColor(getContext(), R.color.table_price_high));
//        }
//
//        return textView;
//    }

    private View renderPower(final Customer customer, final ViewGroup parentView) {
        final View view = getLayoutInflater().inflate(R.layout.table_cell_power, parentView, false);
        final TextView kwView = (TextView) view.findViewById(R.id.kw_view);
        final TextView psView = (TextView) view.findViewById(R.id.ps_view);

//        kwView.setText(format(Locale.ENGLISH, "%d %s", customer.getKw(), getContext().getString(R.string.kw)));
//        psView.setText(format(Locale.ENGLISH, "%d %s", customer.getPs(), getContext().getString(R.string.ps)));

        return view;
    }

    private View renderCatName(final Customer customer) {
        return renderString(customer.getName());
    }

    private View renderProducerLogo(final Customer customer, final ViewGroup parentView) {
        final View view = getLayoutInflater().inflate(R.layout.table_cell_image, parentView, false);
        final ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
//        imageView.setImageResource(customer.getProducer().getLogo());
        return view;
    }

    private View renderString(final String value) {
        final TextView textView = new TextView(getContext());
        textView.setText(value);
        textView.setPadding(20, 10, 20, 10);
        textView.setTextSize(TEXT_SIZE);
        return textView;
    }

    private static class CustomerNameUpdater implements TextWatcher {

        private Customer customerToUpdate;

        public CustomerNameUpdater(Customer carToUpdate) {
            this.customerToUpdate = carToUpdate;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // no used
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // not used
        }

        @Override
        public void afterTextChanged(Editable s) {
            customerToUpdate.setName(s.toString());
        }
    }

}
