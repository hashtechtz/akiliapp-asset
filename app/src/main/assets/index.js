<html><body>
    <div id="mapdiv"></div>
    <script src="http://www.openlayers.org/api/OpenLayers.js"></script>
    <script>
          map = new OpenLayers.Map("mapdiv");
          map.addLayer(new OpenLayers.Layer.OSM());


          var lonLat = new OpenLayers.LonLat( 39.273982222222  , -6.7694522222222)
                .transform(
                  new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                  map.getProjectionObject() // to Spherical Mercator Projection
                );

          var zoom = 13;
          var markers = new OpenLayers.Layer.Markers( "Markers" );
          map.addLayer(markers);



//      var icon = new OpenLayers.Icon('http://maps.google.com/mapfiles/ms/icons/red-pushpin.png');
     // var icon = new OpenLayers.Icon('mipmap/month.png');

         // markers.addMarker(new OpenLayers.Marker(lonLat,icon));
          //markers.addMarker(new OpenLayers.Marker(lonLat1,icon.clone()));

          map.setCenter (lonLat, zoom);
        </script>
    </body></html>