package com.akiliapp.hashtech.taitaremote;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

public class Transport extends AppCompatActivity {

    MapView map;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transport);

        final Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        map = (MapView) findViewById(R.id.map);
        final IMapController mapController = map.getController();
        mapController.setZoom(18);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        GeoPoint startPoint = new GeoPoint(-6.2174222222, 39.447435);
        Marker startMarker = new Marker(map);
        startMarker.setIcon(ctx.getResources().getDrawable(R.drawable.ic_bodaboda));
        startMarker.setDraggable(true);
//        double angl = Double.parseDouble(userResponses.get(n).getCourse());
//        float angle = (float) -angl;
        startMarker.setRotation(-90);
        startMarker.setPosition(startPoint);
        map.getOverlayManager().clear();
        map.getOverlays().add(startMarker);
        map.invalidate();
        mapController.setCenter(startPoint);
    }
}
