package com.akiliapp.hashtech.taitaremote;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class SplashScreen_Activity extends AppCompatActivity {

    private ImageView logo1;
    private TextView textView;
    public static String sessionId = "";
    public static String user_session = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        logo1 = findViewById(R.id.logo);
        textView = findViewById(R.id.title1);


        Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.fade_amin);
        myAnim.setRepeatMode(Animation.INFINITE);
        logo1.startAnimation(myAnim);
        textView.startAnimation(myAnim);


     //   final Intent i = new Intent(this,MainActivity.class);



        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(3000);
                    // SharedPreferences shared = getSharedPreferences("Mac", Context.MODE_PRIVATE);
                    SharedPreferences shared = getSharedPreferences("maco", Context.MODE_PRIVATE);
                    String val = shared.getString("reg_no", "");
                    if (val.length() == 0) {

                        Intent intent = new Intent(SplashScreen_Activity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();


                    } else {

//                        Intent intent = new Intent(SplashScreen_Activity.this, MenuActivity.class);
//                        intent.putExtra("EXTRA_SESSION_ID", val);
//                        startActivity(intent);
                        Intent intent = new Intent(SplashScreen_Activity.this, OpenLayer_Activity.class);
                        intent.putExtra("EXTRA_SESSION_ID", val);
                        startActivity(intent);
//                        Intent intent = new Intent(SplashScreen_Activity.this, Transport.class);
//                        intent.putExtra("EXTRA_SESSION_ID", val);
//                        startActivity(intent);
//                        Bundle bundle = new Bundle();
//                        bundle.putString("reg_no", "EXTRA_SESSION_ID");
//                        VehicleFragment myFrag = new VehicleFragment();
//                        myFrag.setArguments(bundle);
                        finish();

                        sessionId = val;

                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        };

        thread.start();
    }

}
