package com.akiliapp.hashtech.taitaremote;

public class Vehicledetails_POJO {


        String name,uniqueId,plate_number,device_model,lastValidLatitude,lastValidLongitude,registration_number;


        public String getName() {

            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String to_trip) {
            this.uniqueId = uniqueId;
        }

        public String getPlate_number() {
            return plate_number;
        }

        public void setPlate_number(String plate_number) {
            this.plate_number = plate_number;
        }

        public String getDevice_model() {

            return device_model;
        }

        public void setDevice_model(String device_model) {
            this.device_model = device_model;
        }

        public String getLastValidLatitude() {
            return lastValidLatitude;
        }

        public void setLastValidLatitude(String lastValidLatitude) {
            this.lastValidLatitude = lastValidLatitude;
        }

        public String getLastValidLongitude() {
            return lastValidLongitude;
        }

        public void setLastValidLongitude(String lastValidLongitude) {
            this.lastValidLongitude = lastValidLongitude;
        }
        public String getRegistration_number() {
            return registration_number;
        }

        public void setRegistration_number(String registration_number) {
            this.registration_number = registration_number;
        }


}
