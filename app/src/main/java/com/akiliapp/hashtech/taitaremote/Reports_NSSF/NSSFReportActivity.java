package com.akiliapp.hashtech.taitaremote.Reports_NSSF;

import android.os.Bundle;

import android.widget.Toast;


import com.akiliapp.hashtech.taitaremote.R;
import com.akiliapp.hashtech.taitaremote.Reports_NSSF.data.Customer;
import com.akiliapp.hashtech.taitaremote.Reports_NSSF.data.DataFactory;

import java.util.List;
import java.util.Random;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.SwipeToRefreshListener;
import de.codecrafters.tableview.listeners.TableDataClickListener;
import de.codecrafters.tableview.listeners.TableDataLongClickListener;


public class NSSFReportActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportsnssf);

//        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        if (toolbar != null) {
//            setSupportActionBar(toolbar);
//        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
//      ab.setIcon(R.drawable.ic_boda_icon);
        ab.setTitle("Taarifa za Michango (NSSF)");

        final TableView<Customer> customerTableView = (TableView<Customer>) findViewById(R.id.tableView);
        if (customerTableView != null) {
            final CustomerTableDataAdapter customerTableDataAdapter = new CustomerTableDataAdapter(this, DataFactory.createCustomerList(), customerTableView);
            customerTableView.setDataAdapter(customerTableDataAdapter);
            customerTableView.addDataClickListener(new CustomerClickListener());
            customerTableView.addDataLongClickListener(new CustomerLongClickListener());
            customerTableView.setSwipeToRefreshEnabled(true);
            customerTableView.setSwipeToRefreshListener(new SwipeToRefreshListener() {
                @Override
                public void onRefresh(final RefreshIndicator refreshIndicator) {
                    customerTableView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final Customer randomCustomer = getRandomCustomer();
                            customerTableDataAdapter.getData().add(randomCustomer);
                            customerTableDataAdapter.notifyDataSetChanged();
                            refreshIndicator.hide();
                            Toast.makeText(NSSFReportActivity.this, "Added: " + randomCustomer, Toast.LENGTH_SHORT).show();
                        }
                    }, 3000);
                }
            });
        }
    }

    private Customer getRandomCustomer() {
        final List<Customer> customerList = DataFactory.createCustomerList();
        final int randomCustomerIndex = Math.abs(new Random().nextInt() % customerList.size());
        return customerList.get(randomCustomerIndex);
    }

    private class CustomerClickListener implements TableDataClickListener<Customer> {

        @Override
        public void onDataClicked(final int rowIndex, final Customer clickedData) {
            final String customerString = "Click: " + clickedData.getProducer().getName() + " " + clickedData.getName();
            Toast.makeText(NSSFReportActivity.this, customerString, Toast.LENGTH_SHORT).show();
        }
    }

    private class CustomerLongClickListener implements TableDataLongClickListener<Customer> {

        @Override
        public boolean onDataLongClicked(final int rowIndex, final Customer clickedData) {
            final String customerString = "Long Click: " + clickedData.getProducer().getName() + " " + clickedData.getName();
            Toast.makeText(NSSFReportActivity.this, customerString, Toast.LENGTH_SHORT).show();
            return true;
        }
    }
}
