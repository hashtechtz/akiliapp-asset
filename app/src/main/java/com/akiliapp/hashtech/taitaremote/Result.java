package com.akiliapp.hashtech.taitaremote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("plate_number")
    @Expose
    private String plateNumber;
    @SerializedName("type_vehicle")
    @Expose
    private String typeVehicle;
    @SerializedName("model")
    @Expose
    private String model;

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getTypeVehicle() {
        return typeVehicle;
    }

    public void setTypeVehicle(String typeVehicle) {
        this.typeVehicle = typeVehicle;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }





}
